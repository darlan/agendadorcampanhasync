FROM maven:3.6.0-jdk-8
WORKDIR /AgendadorCampanha
RUN git clone https://gitlab.com/darlan/agendadorcampanhasync.git /AgendadorCampanha
RUN cd /AgendadorCampanha
RUN mvn compile --batch-mode && mvn verify --batch-mode 
CMD ["java", "-jar", "./target/AgendadorAutomatico-jar-with-dependencies.jar"]
