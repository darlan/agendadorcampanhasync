package com.org.agendadores;

import com.org.daos.taxaConversao.TaxaConversaoVezDao;
import com.org.modelos.Cliente;
import com.org.utis.UtilBancoDados;
import com.org.utis.cliente.UtilCliente;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class AgendadorFilaTaxaConversao implements Job {
    
    private final List<String> CLIENTS = Arrays.asList("lepostiche");
    
    private static final String CRON_EXPRESSION = "0 0 5 * * ?"; //Todo dia as 5 da manhã
    //private static final String CRON_EXPRESSION = "0 */1 * ? * *"; // A cada 1 minuto

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        List<Cliente> clientes = UtilCliente.getClientesAtivos();

        if (clientes != null) {
            clientes.forEach((c) -> {
                if (CLIENTS.contains(c.getNmBanco())){
                    executeClient(c);
                }
            });
        }
    }
    
    private void executeClient(Cliente cliente) {
        try (Connection connection = UtilBancoDados.getConnection(AgendadorFilaTaxaConversao.class, cliente)) {
            TaxaConversaoVezDao taxaConversaoVezDao = new TaxaConversaoVezDao(connection);
            taxaConversaoVezDao.updateListAttendance();
            connection.commit();            
        } catch (SQLException e) {
            Logger.getLogger(AgendadorFilaTaxaConversao.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public static JobDetail getJob() {
        return JobBuilder
                .newJob(AgendadorFilaTaxaConversao.class)
                .withIdentity(AgendadorFilaTaxaConversao.class.getName(), AgendadorFilaTaxaConversao.class.getName())
                .build();
    }
    
    public static Trigger getTrigger() {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(AgendadorFilaTaxaConversao.class.getName() + "TRIGGER", AgendadorFilaTaxaConversao.class.getName())
                .withSchedule(CronScheduleBuilder.cronSchedule(CRON_EXPRESSION))
                .build();
    }
    
}
