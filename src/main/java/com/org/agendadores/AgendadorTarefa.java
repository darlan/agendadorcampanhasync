package com.org.agendadores;

import com.org.modelos.Cliente;
import com.org.servicos.campanha.CampanhaServico;
import com.org.utis.UtilBancoDados;
import com.org.utis.cliente.UtilCliente;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

@DisallowConcurrentExecution
public class AgendadorTarefa implements Job {

    private static final String CRON_EXPRESSION = "0 0 3 * * ?"; //Todo dia as 3 da manhã
//    private static final String CRON_EXPRESSION = "0 */1 * ? * *"; // A cada 1 minuto

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        List<Cliente> clientes = UtilCliente.getClientesAtivos();
        
        if (clientes != null) {
            clientes.forEach((c) -> {
                executeClient(c);
            });
        }
    }

    private void executeClient(Cliente cliente) {
        try (Connection connection = UtilBancoDados.getConnection(AgendadorTarefa.class, cliente)) {
            CampanhaServico campanhaServico = new CampanhaServico(connection, cliente);
            campanhaServico.criarCampanhas();
            campanhaServico.cancelarCampanhas();
        } catch (SQLException e) {
            Logger.getLogger(AgendadorTarefa.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public static JobDetail getJob() {
        return JobBuilder
                .newJob(AgendadorTarefa.class)
                .withIdentity(AgendadorTarefa.class.getName(), AgendadorTarefa.class.getName())
                .build();
    }

    public static Trigger getTrigger() {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(AgendadorTarefa.class.getName() + "TRIGGER", AgendadorTarefa.class.getName())
                .withSchedule(CronScheduleBuilder.cronSchedule(CRON_EXPRESSION))
                .build();
    }

}
