package com.org.agendadores;

import com.org.enums.RestEnum;
import com.org.utis.UtilServlet;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class AgendadorWikiVigentes implements Job {
    
    private static final String CRON_EXPRESSION = "0 0 0/1 * * ?"; //A cada 1 hora
//    private static final String CRON_EXPRESSION = "0 */1 * ? * *"; // A cada 1 minuto
    
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {
            LinkedHashMap<String, String> headers = new LinkedHashMap<>();
            headers.put(RestEnum.KEY.getKey(), "73-teammove");
            headers.put(RestEnum.TOKEN.getKey(), "d0c5ebda0bd5f43556be7ba4ef6fa4206a6324c410d7736cd414f0a144feb332");
            
            UtilServlet.callGet(UtilServlet.getUrlEndPoint() + RestEnum.WIKI_NOTIFICACOES.getKey(), null, null, headers);
        } catch (Exception ex) {
            Logger.getLogger(AgendadorWikiVigentes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static JobDetail getJob() {
        return JobBuilder
                .newJob(AgendadorWikiVigentes.class)
                .withIdentity(AgendadorWikiVigentes.class.getName(), AgendadorWikiVigentes.class.getName())
                .build();
    }

    public static Trigger getTrigger() {
        return TriggerBuilder
                .newTrigger()
                .withIdentity(AgendadorWikiVigentes.class.getName() + "TRIGGER", AgendadorWikiVigentes.class.getName())
                .withSchedule(CronScheduleBuilder.cronSchedule(CRON_EXPRESSION))
                .build();
    }
}
