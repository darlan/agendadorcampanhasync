package app.tmove.boladao;

import app.tmove.boladao.annotation.Column;
import app.tmove.boladao.annotation.Exclude;
import app.tmove.boladao.annotation.PrimaryKey;
import app.tmove.boladao.enums.Database;
import app.tmove.boladao.interfaces.DaoMethods;
import app.tmove.boladao.models.ColumnDB;
import app.tmove.boladao.models.DaoField;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Boladao<T> {

    private final String table;
    private Database databaseSyntax = Database.POSTGRES_v95; 
    
    private Connection connection = null;
    private final boolean keepConnectionAlive;
    
    private final List<DaoField> primaryKeys = new ArrayList<>();
    private final List<DaoField> columns = new ArrayList<>();
    
    private String sqlInsert;
    private String sqlUpdate;
    private String sqlInsertOrUpdate;
    
    private String sqlDeleteWhere;
    private String sqlSelectColumns;
    private String sqlSelectFrom;
    private String sqlSelectWhere = "1 = 1";
    private String sqlSelectWhereObject;
    
    private DaoField primaryKeyAutoIncrement = null;
    
    private DaoMethods.CreateConnection createConnection;
    
    public Boladao(Connection connection, String table, boolean keepConnectionAlive, DaoMethods.CreateConnection createConnection, Database databaseSyntax) {
        this.table = table;
        this.connection = connection;
        this.keepConnectionAlive = keepConnectionAlive;
        this.createConnection = createConnection;
        
        if (databaseSyntax != null) {
            this.databaseSyntax = databaseSyntax;
        }
        
        setSqlOperations();
    }
    
    public Boladao(String table, DaoMethods.CreateConnection createConnection, Database databaseSyntax) {
        this(null, table, false, createConnection, databaseSyntax);
    }
    
    public Boladao(String table, DaoMethods.CreateConnection createConnection) {
        this(null, table, false, createConnection, null);
    }
    
    public Boladao(String table, Database databaseSyntax) {
        this(null, table, false, null, databaseSyntax);
    }
    
    public Boladao(String table) {
        this(null, table, false, null, null);
    }
    
    public Boladao(Connection connection, String table, Database databaseSyntax) {
        this(connection, table, true, null, databaseSyntax);
    }
    
    public Boladao(Connection connection, String table) {
        this(connection, table, true, null, null);
    }
    
    /*
    Métodos principais da classe
    */
    protected abstract T populate(ResultSetDao rs) throws SQLException;
    protected abstract void insert(PreparedStatementDao ps, T object) throws SQLException;
    protected abstract void update(PreparedStatementDao ps, T object) throws SQLException;
    
    protected void upsert(PreparedStatementDao ps, T object) throws SQLException {
        //Sobrescrever esse método caso utiliza o upsert com syntax onde vai ser feito um exists na tabela. Ex: sybase, postgres < 9.5
        insert(ps, object);
    }
    
    public final boolean delete(T object) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        //Já faz o delete de forma genérica pelas chaves primarias
        try {
            openConnection();
            
            List<Object> params = new ArrayList();

            for (DaoField key : primaryKeys) {
                params.add(getColumnValue(key, object));
            }

            return deleteObject(sqlDeleteWhere, params.toArray(new Object[]{params.size()}));
        } finally {
            closeConnection();
        }
    }
    
    public final boolean deleteAll() throws SQLException {
        try {
            openConnection();
            
            String sql = "delete from " + getTable();
            return executeUpdate(sql) > 0;
        } finally {
            closeConnection();
        }
    }
    
    public final boolean delete(String where) throws SQLException {
        try {
            openConnection();
            
            String sql = "delete from " + getTable() + " " + where;
            return executeUpdate(sql) > 0;
        } finally {
            closeConnection();
        }
    }
    
    public final T getObject(T object) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        List<Object> params = new ArrayList();

        for (DaoField key : primaryKeys) {
            params.add(getColumnValue(key, object));
        }

        return getObject(sqlSelectWhereObject, params.toArray(new Object[]{params.size()}));
    }
    
    public final BoladaoQuery query() throws SQLException {
        return new BoladaoQuery(getConnection(), sqlSelectColumns, sqlSelectFrom, sqlSelectWhere, getDatabaseSyntax(), new DaoMethods.Populate() {
            @Override
            public T code(ResultSetDao rs) throws SQLException {
                return populate(rs);
            }
        })
                .setCreateConnection(createConnection);
    }
    
    public final int insert(T object) throws SQLException {
        try {
            openConnection();
            
            int rowCount = insertSingle(object);
            
            commitConnection();
            return rowCount;
        } finally {
            closeConnection();
        }
    }
    
    public final int update(T object) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        try {
            openConnection();
            
            int rowCount = updateSingle(object);
            
            commitConnection();
            return rowCount;
        } finally {
            closeConnection();
        }
    }
    
    public final int insertOrUpdate(T object) throws SQLException {
        try {
            openConnection();
            
            int rowCount = insertOrUpdateSingle(object);
            
            commitConnection();
            return rowCount;
        } finally {
            closeConnection();
        }
    }

    public final void insertWithResult(T object) throws SQLException {
        try {
            openConnection();
            
            try (PreparedStatementDao ps = getStatementGeneratedKeys(sqlInsert)) {
                System.out.println(object);
                System.out.println(sqlInsert);
                ps.resetIndex();
                insert(ps, object);
                ps.executeUpdate();
                
                setAutoIncrementValue(object, ps);
            }
        } finally {
            closeConnection();
        }
    }

    public final int upsert(T object) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        try {
            openConnection();
            
            int rowCount = updateSingle(object);
            if (rowCount == 0) {
                rowCount = insertSingle(object);
            }

            commitConnection();
            return rowCount;
        } finally {
            closeConnection();
        }
    }

    public final int insert(List<T> listObject) throws SQLException {
        try {
            openConnection();
            
            try (PreparedStatementDao ps = getStatement(sqlInsert)) {
                
                for (T object : listObject) {
                    ps.resetIndex();
                    insert(ps, object);
                    ps.addBatch();
                }

                int[] result = ps.executeBatch();
                
                commitConnection();
                return result.length;
            }
        } finally {
            closeConnection();
        }
    }

    public final int update(List<T> listObject) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        try {
            openConnection();
            
            try (PreparedStatementDao ps = getStatement(sqlUpdate)) {
                
                for (T object : listObject) {
                    ps.resetIndex();
                    update(ps, object);
                    ps.addBatch();
                }

                int[] result = ps.executeBatch();
                
                commitConnection();
                return result.length;
            }
        } finally {
            closeConnection();
        }
    }
    
    public final int insertOrUpdate(List<T> listObject) throws SQLException {
        try {
            openConnection();
            
            try (PreparedStatementDao ps = getStatement(sqlInsertOrUpdate)) {
                
                for (T object : listObject) {
                    ps.resetIndex();
                    insert(ps, object);
                    ps.addBatch();
                }

                int[] result = ps.executeBatch();
                
                commitConnection();
                return result.length;
            }
        } finally {
            closeConnection();
        }
    }

    public final int upsert(List<T> listObject) throws SQLException {
        if (getTable() != null && primaryKeys.isEmpty()) {
            throw new RuntimeException("Nao foi especificada nenhuma chave primaria para o modelo da tabela " + table);
        }
        
        try {
            openConnection();
            
            try (PreparedStatementDao psInsert = getStatement(sqlInsert + getDatabaseSyntax().onConflict(getTable(), primaryKeys));
                    PreparedStatementDao psUpdate = getStatement(sqlUpdate);) {
                
                for (T object : listObject) {
                    psInsert.resetIndex();
                    psUpdate.resetIndex();
                    
                    update(psUpdate, object);
                    upsert(psInsert, object);
                    
                    psUpdate.addBatch();
                    psInsert.addBatch();
                }

                int result = psUpdate.executeBatch().length;
                result += psInsert.executeBatch().length;
                
                commitConnection();
                return result;
            }
        } finally {
            closeConnection();
        }
    }
    
    public final Integer nextVal() throws SQLException {
        try (PreparedStatementDao ps = new PreparedStatementDao(getConnection().prepareStatement("select nextval('"+getTable()+"_id_seq'::regclass)")))  {
            try (ResultSetDao rs = new ResultSetDao(ps.executeQuery())) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    return null;
                }
            }
        }
    }
    
    /*
    Métodos que executam o comandos internos
    */   
    
    protected final T getObject(String where, Object... params) throws SQLException {
        return new BoladaoQuery(getConnection(), sqlSelectColumns, sqlSelectFrom, sqlSelectWhere, getDatabaseSyntax(), new DaoMethods.Populate() {
            @Override
            public T code(ResultSetDao rs) throws SQLException {
                return populate(rs);
            }
        }).where(where).params(params).setCreateConnection(createConnection).getObject();
    }
    
    protected final T getObject(String where, List<ColumnDB> params) throws SQLException {
        try {
            openConnection();
            
            return new BoladaoQuery(getConnection(), sqlSelectColumns, sqlSelectFrom, sqlSelectWhere, getDatabaseSyntax(), new DaoMethods.Populate() {
                @Override
                public T code(ResultSetDao rs) throws SQLException {
                    return populate(rs);
                }
            }).where(where).paramsWithType(params.toArray(new ColumnDB[params.size()])).getObject();
        }
        finally {
            closeConnection();
        }
    }
    
    protected final boolean deleteObject(String where, Object... params) throws SQLException {
        String sql = "delete from " + getTable() + " " + (where != null ? "where " + where : "");
        return executeUpdate(sql, params) > 0;
    }
    protected final boolean deleteObject(String where, List<ColumnDB> params) throws SQLException {
        String sql = "delete from " + getTable() + " " + (where != null ? "where " + where : "");
        return executeUpdate(sql, params) > 0;
    }
    
    private int insertSingle(T object) throws SQLException {
        try (PreparedStatementDao ps = getStatement(sqlInsert)) {
            ps.resetIndex();
            insert(ps, object);

            int retorno = ps.executeUpdate();
            
            setAutoIncrementValue(object, ps);
            
            return retorno;
        }
    }
    
    private int updateSingle(T object) throws SQLException {
        try (PreparedStatementDao ps = getStatement(sqlUpdate)) {
            ps.resetIndex();
            update(ps, object);
            
            return ps.executeUpdate();
        }
    }
    
    private int insertOrUpdateSingle(T object) throws SQLException {        
        try (PreparedStatementDao ps = getStatement(sqlInsertOrUpdate)) {
            ps.resetIndex();
            insert(ps, object);
            
            int retorno = ps.executeUpdate();
            
            setAutoIncrementValue(object, ps);
            
            return retorno;
        }
    }
    
    /*
    Métodos genéricos
    */
    private void setAutoIncrementValue(T object, PreparedStatementDao ps) throws SQLException {
        if (primaryKeyAutoIncrement != null) {
            try (java.sql.ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    try {
                        object.getClass().getMethod(primaryKeyAutoIncrement.getSetMethod(), primaryKeyAutoIncrement.getField().getType()).invoke(object, rs.getObject(primaryKeyAutoIncrement.getSqlName()));
                    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        throw new SQLException("Problemas ao realizar métodos genéricos", ex);
                    }

                    commitConnection();
                }
            }
        }
    }
    
    protected final PreparedStatementDao getStatement(String sql) throws SQLException {
        if (getConnection() == null) {
            throw new SQLException("Conexão com o banco foi nula!");
        }
        return new PreparedStatementDao(getConnection().prepareStatement(sql));
    }
    
    protected final PreparedStatementDao getStatementGeneratedKeys(String sql) throws SQLException {
        if (getConnection() == null) {
            throw new SQLException("Conexão com o banco foi nula!");
        }
        return new PreparedStatementDao(getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS));
    }
    
    protected final boolean execute(String SQL) throws SQLException {
        try (PreparedStatementDao ps = getStatement(SQL)) {
            return ps.execute();
        }
    }

    protected final boolean execute(String SQL, Object... params) throws SQLException {
        try (PreparedStatementDao ps = getStatement(SQL)) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] != null) {
                    ps.setObject((i + 1), params[i]);
                }
                else {
                    ps.setNull((i + 1), java.sql.Types.VARCHAR); //É o tipo mais genérico
                }
            }
            return ps.execute();
        }
    }

    protected final boolean execute(String SQL, List<ColumnDB> params) throws SQLException {
        try (PreparedStatementDao ps = getStatement(SQL)) {
            if (params != null) {
                for (int i = 0; i < params.size(); i++) {
                    ColumnDB object = params.get(i);

                    setColumnValue(ps, object, (i + 1));
                }
            }
            return ps.execute();
        }
    }
    
    protected final int executeUpdate(String SQL, Object... params) throws SQLException {
        try (PreparedStatementDao ps = getStatement(SQL)) {
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    if (params[i] != null) {
                        ps.setObject((i + 1), params[i]);
                    } else {
                        ps.setNull((i + 1), Types.VARCHAR);
                    }
                }
            }
            return ps.executeUpdate();
        }
    }

    protected final int executeUpdate(String SQL, List<ColumnDB> params) throws SQLException {
        try (PreparedStatementDao ps = getStatement(SQL)) {
            if (params != null) {
                for (int i = 0; i < params.size(); i++) {
                    ColumnDB object = params.get(i);

                    setColumnValue(ps, object, (i + 1));
                }
            }
            return (ps.executeUpdate());
        }
    }
    
    protected final java.sql.ResultSet executeQuery(String SQL, Object... params) throws SQLException {
        PreparedStatementDao ps = getStatement(SQL);
        for (int i = 0; i < params.length; i++) {
            if (params[i] != null) {
                ps.setObject((i + 1), params[i]);
            }
            else {
                ps.setNull((i + 1), java.sql.Types.VARCHAR); //É o tipo mais genérico
            }
        }
        return (ps.executeQuery());
    }
    
    protected final java.sql.ResultSet executeQuery(String SQL, List<ColumnDB> params) throws SQLException {
        PreparedStatementDao ps = getStatement(SQL);
        if (params != null) {
            for (int i = 0; i < params.size(); i++) {
                ColumnDB object = params.get(i);

                setColumnValue(ps, object, (i + 1));
            }
        }
        return (ps.executeQuery());
    }
    
    private Object getColumnValue(DaoField field, T object) throws SQLException {
        try {
            Method getValue = object.getClass().getDeclaredMethod(field.getGetMethod(), (Class<?>[]) null);
            Object o = getValue.invoke(object, (Object[]) null);
            
            if (o == null) {
                throw new SQLException("Não foi atribuido nenhum valor para a chave primária '" + field.getName() + "'");
            }

            return o;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Boladao.class.getName()).log(Level.SEVERE, "Problema ao recuperar valores no método 'delete'", ex);
            return null;
        }
    }
    
    private void setColumnValue(PreparedStatementDao ps, ColumnDB object, int position) throws SQLException {
        if (object.getValor() != null) {
            switch (object.getType()) {
                case Types.VARCHAR: {
                    ps.setString(position, (String) object.getValor());
                    break;
                }

                case Types.INTEGER: {
                    ps.setInt(position, (Integer) object.getValor());
                    break;
                }

                case Types.NUMERIC: {
                    ps.setDouble(position, (Double) object.getValor());
                    break;
                }

                case Types.DATE: {
                    ps.setDate(position, new java.sql.Date(((Date) object.getValor()).getTime()));
                    break;
                }

                case Types.TIMESTAMP: {
                    ps.setTimestamp(position, new java.sql.Timestamp(((Date) object.getValor()).getTime()));
                    break;
                }
                
                case Types.ARRAY: {
                    ps.setArray(position, (Array) object.getValor());
                    break;
                }
            }
        } else {
            ps.setNull(position, object.getType());
        }
    }
    
    /*
    *
    *Métodos do construtor
    *
    */
    private void setSqlOperations() {
        if (getTable() == null) {
            return;
        }
        
        StringBuilder insertColumns = new StringBuilder("insert into " + getTable() + " (");
        StringBuilder insertValues = new StringBuilder("values(");
        
        StringBuilder updateColumns = new StringBuilder("update " + getTable() + " set ");
        StringBuilder updateWhere = new StringBuilder("where ");
        
        StringBuilder onConflictPrimaryKeys = new StringBuilder("on conflict (");
        StringBuilder onConflictColums = new StringBuilder("do update set ");
        
        StringBuilder selectColumns = new StringBuilder();
        StringBuilder selectWhere = new StringBuilder();
        
        StringBuilder deleteWhere = new StringBuilder();
        
        boolean primeiroInsert = true;
        boolean primeiroUpdateField = true;
        boolean primeiroUpdateWhere = true;
        boolean primeiroDeleteWhere = true;
        boolean primeiroSelectColumn = true;
        boolean primeiroSelectWhere = true;
        
        Class<T> c = ((Class)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        for (Field field : c.getDeclaredFields()) {
            String name = field.getName();
            String nameNormalized = normalizeFieldToSql(name);
            
            Exclude annExclude = null;
            PrimaryKey annPrimaryKey = null;
            Column annColumn = null;
            
            //Anotação para saber se o campo é utilizado nas operações
            if (field.isAnnotationPresent(PrimaryKey.class)) {
                Annotation annotation = field.getAnnotation(PrimaryKey.class);
                annPrimaryKey = (PrimaryKey) annotation;
            }
            
            //Anotação para saber se o campo é chave primária, e se é utilizado nos wheres
            if (field.isAnnotationPresent(Exclude.class)) {
                Annotation annotation = field.getAnnotation(Exclude.class);
                annExclude = (Exclude) annotation;
            }
            
            //Anotação para saber se existe alguma peculiaridade nos registros da coluna
            if (field.isAnnotationPresent(Column.class)) {
                Annotation annotation = field.getAnnotation(Column.class);
                annColumn = (Column) annotation;
                if (annColumn.name() != null && !annColumn.name().equals("")) {
                    nameNormalized = annColumn.name();
                }
            }
            
            //Adiciona na lista de campos
            String getMethodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
            String setMethodName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
            DaoField daoField = new DaoField(field, name, nameNormalized, getMethodName, setMethodName, annExclude, annPrimaryKey, annColumn);
            if (annPrimaryKey == null) {
                columns.add(daoField);
            }
            else {
                primaryKeys.add(daoField);
                if (annPrimaryKey.autoIncrement()) {
                    if (primaryKeyAutoIncrement != null) {
                        throw new RuntimeException("Não é possível ter duas chaves primárias com auto increment");
                    }
                    primaryKeyAutoIncrement = daoField;
                }
            }
            
            //Adiciona o campo para o sql de inserção
            if ((annPrimaryKey == null || !annPrimaryKey.autoIncrement()) && (annExclude == null || !annExclude.excludeFromInsert())) {
                if (annColumn == null || !annColumn.custom()) { //Não utiliza o campo caso ele for custom, que pode ser expressão o valor fixo que não pertence a tabela
                    if (!primeiroInsert) {
                        insertColumns.append(", ");
                        insertValues.append(", ");
                    }

                    insertColumns.append(nameNormalized);
                    if (annColumn != null) {
                        insertValues.append(annColumn.insertValue());
                    }
                    else {
                        insertValues.append("?");
                    }

                    primeiroInsert = false;
                }
            }
            
            //Adiciona o campo para o sql de atualização
            if (annExclude == null || !annExclude.excludeFromUpdate()) {
                if (annColumn == null || !annColumn.custom()) { //Não utiliza o campo caso ele for custom, que pode ser expressão o valor fixo que não pertence a tabela
                    if (annPrimaryKey != null) {
                        if (!primeiroUpdateWhere) {
                            updateWhere.append(" and ");
                            onConflictPrimaryKeys.append(",");
                        }

                        updateWhere.append(nameNormalized).append("=?");
                        onConflictPrimaryKeys.append(nameNormalized);

                        primeiroUpdateWhere = false;
                    }
                    else {
                        if (!primeiroUpdateField) {
                            updateColumns.append(", ");
                            onConflictColums.append(", ");
                        }

                        if (annColumn != null && annColumn.updateValue() != null) {
                            updateColumns.append(nameNormalized).append("=").append(annColumn.updateValue());
                        }
                        else {
                            updateColumns.append(nameNormalized).append("=?");
                        }
                        onConflictColums.append(nameNormalized).append("=excluded.").append(nameNormalized);

                        primeiroUpdateField = false;
                    }
                }
            }
           
            //Adiciona o campo para o sql de pesquisa
            if (annExclude == null || !annExclude.excludeFromSelect()) {
                if (!primeiroSelectColumn) {
                    selectColumns.append(", ");
                }
                if (annColumn == null || !annColumn.custom()) {
                    selectColumns.append(getTable()).append(".");
                }
                selectColumns.append(nameNormalized);
                primeiroSelectColumn = false;
            }
            
            //Adiciona a primary key para o sql de pesquisa específica
            if (annPrimaryKey != null) {
                if (!primeiroSelectWhere) {
                    selectWhere.append(" and ");
                }
                selectWhere.append(nameNormalized).append("=?");
                primeiroSelectWhere = false;
            }
            
            //Faz o where com a primary key do delete
            if (annPrimaryKey != null) {
                if (!primeiroDeleteWhere) {
                    deleteWhere.append(" and ");
                }
                deleteWhere.append(nameNormalized).append("=?");
                primeiroDeleteWhere = false;
            }
        }
        
        onConflictPrimaryKeys.append(")");
        
        String insertReturning = "";
        if (primaryKeyAutoIncrement != null) {
            insertReturning = " returning " + primaryKeyAutoIncrement.getSqlName();
        }
        
        sqlInsert = insertColumns.toString() + ") " + insertValues.toString() + ")" + insertReturning;
        sqlUpdate = updateColumns.toString() + " " + updateWhere.toString();
        sqlInsertOrUpdate = insertColumns.toString() + ") " + insertValues.toString() + ") " + onConflictPrimaryKeys + " " + onConflictColums + insertReturning;
        
        sqlDeleteWhere = deleteWhere.toString();
        setSqlSelectColumns(selectColumns.toString());
        setSqlSelectFrom(table);
        sqlSelectWhereObject = selectWhere.toString();
    }
    
    private static String normalizeFieldToSql(String text) {
        Pattern p = Pattern.compile( "([A-Z])" );
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "_" + m.group(1).toLowerCase());
        }
        m.appendTail(sb);
        return sb.toString();
    }
    
    /*
    *
    * Métodos Auxiliares
    *
    */
    private Connection createConnection() throws SQLException {
        if (createConnection == null) {
            throw new UnsupportedOperationException("Você deve passar uma interface createConnection para criar a conexão.");
        }
        else {
            return createConnection.code();
        }
    }
    
    private void openConnection() throws SQLException {
        if (!keepConnectionAlive) {
            setConnection(createConnection());
        }
    }
    
    private void closeConnection() throws SQLException {
        if (!keepConnectionAlive) {
            if (!getConnection().getAutoCommit()) {
                getConnection().rollback();
            }
            getConnection().close();
        }
    }
    
    private void commitConnection() throws SQLException {
        if (!keepConnectionAlive) {
            if (!getConnection().getAutoCommit()) {
                getConnection().commit();
            }
        }
    }
    
    /*
    *
    * GETTERS and SETTERS
    *
    */
    
    public final String getTable() {
        return table;
    }

    public final Connection getConnection() {
        return connection;
    }

    public final void setConnection(Connection connection) {
        this.connection = connection;
    }
    
    public final void setSqlSelectColumns(String sqlSelectColumns) {
        this.sqlSelectColumns = sqlSelectColumns;
    }

    public final void setSqlSelectFrom(String sqlSelectFrom) {
        this.sqlSelectFrom = sqlSelectFrom;
    }

    public final void setSqlSelectWhere(String sqlSelectWhere) {
        this.sqlSelectWhere = sqlSelectWhere;
    }

    public final Database getDatabaseSyntax() {
        return databaseSyntax;
    }

    public final void setDatabaseSyntax(Database databaseSyntax) {
        this.databaseSyntax = databaseSyntax;
    }
}
