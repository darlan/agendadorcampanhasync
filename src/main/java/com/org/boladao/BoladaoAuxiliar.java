package app.tmove.boladao;

import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import java.sql.Connection;
import java.sql.SQLException;

public class BoladaoAuxiliar extends Boladao<Object> {

    public BoladaoAuxiliar(Connection connection) {
        super(connection, null);
    }

    @Override
    protected Object populate(ResultSetDao rs) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    protected void insert(PreparedStatementDao ps, Object object) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    protected void update(PreparedStatementDao ps, Object object) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

}
