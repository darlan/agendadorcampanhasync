package app.tmove.boladao;

import app.tmove.boladao.enums.Database;
import app.tmove.boladao.interfaces.DaoMethods;
import app.tmove.boladao.models.ColumnDB;
import app.tmove.boladao.utils.ResultSetDao;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public final class BoladaoQuery {
    
    private Connection connection;
    private final Database databaseSyntax;
    
    private String columns;
    private String from;
    private String where;
    private final String whereDefault;
    private String groupBy;
    private String orderBy;
    private Integer limit;
    private Integer limitOffSet;
    private List<Object> params = new ArrayList<>();
    private List<ColumnDB> paramsWithType = new ArrayList<>();
    
    private DaoMethods.Populate populate;
    
    //Usado para recriar a conexão caso a o dao utilize dessa forma
    private DaoMethods.CreateConnection createConnection;
    
    private boolean debug = false;

    public BoladaoQuery(Connection connection, String columns, String from, String whereDefault, Database databaseSyntax, DaoMethods.Populate populate) {
        if (whereDefault == null || whereDefault.equals("")) {
            whereDefault = "1 = 1";
        }
        
        if (databaseSyntax == null) {
            databaseSyntax = Database.POSTGRES_v95;
        }
        
        this.connection = connection;
        this.columns = columns;
        this.from = from;
        this.whereDefault = whereDefault;
        this.databaseSyntax = databaseSyntax;
        this.populate = populate;
    }
    
    public BoladaoQuery(Connection conn) {
        this(conn, null, null, null, null, null);
    }
    
    public BoladaoQuery(Connection conn, Database databaseSyntax) {
        this(conn, null, null, null, databaseSyntax, null);
    }
    
    public BoladaoQuery(Connection conn, String columns, String from, DaoMethods.Populate populate) {
        this(conn, columns, from, null, null, populate);
    }
    
    public BoladaoQuery(Connection conn, String[] columns, String from, DaoMethods.Populate populate) {
        this(conn, Arrays.toString(columns).replace("[", "").replace("]", ""), from, null, null, populate);
    }
    
    public BoladaoQuery(Connection conn, String columns, String from) {
        this(conn, columns, from, null, null, null);
    }
    
    public BoladaoQuery(Connection conn, String[] columns, String from) {
        this(conn, Arrays.toString(columns).replace("[", "").replace("]", ""), from, null, null, null);
    }
    
    /*
    Métodos funcionais da classe
    */
    public final <T extends Object> T getObject() throws SQLException {
        try {
            openConnection();
            
            if (populate == null) {
                throw new SQLException("É necessário informar o método populate para saber qual objeto será recuperado");
            }

            String sql = getSql();

            T object = null;
            try (ResultSetDao rs = new ResultSetDao(getResultSetOriginal(sql))) {
                try {
                    if (rs.next()) {
                        object = (T) populate.code(rs);
                    }
                }
                finally {
                    rs.getStatement().close();
                }
            }

            return object;
            
        } finally {
            closeConnection();
        }
    }
    
    public final <T extends Object> List<T> getList() throws SQLException {
        try {
            openConnection();
            
            if (populate == null) {
                throw new SQLException("É necessário informar o método populate para saber qual objeto será recuperado");
            }

            String sql = getSql();
//            System.out.println("sql -> \n" + sql);
            List<T> list = new ArrayList<>();
            try (ResultSetDao rs = new ResultSetDao(getResultSetOriginal(sql))) {
                try {
                    while (rs.next()) {
                        list.add((T) populate.code(rs));
                    }
                }
                finally {
                    rs.getStatement().close();
                }
            }

            return list;
            
        } finally {
            closeConnection();
        }
    }
    
    public final HashMap<String, Object> getObjectGeneric() throws SQLException {
        String sql = getSql();
        
        DaoMethods.Populate populateGeneric = new DaoMethods.Populate() {
            private ResultSetMetaData metaData = null;
            
            @Override
            public HashMap<String, Object> code(ResultSetDao rs) throws SQLException {
                HashMap<String, Object> map = new HashMap<>();
                
                if (metaData == null) {
                    metaData = rs.getMetaData();
                }

                int count = metaData.getColumnCount();
                for (int i = 1; i <= count; i++) {
                    map.put(metaData.getColumnName(i), rs.getObject(i));
                }
                
                return map;
            }
        };
        
        HashMap<String, Object> object = null;
        try (ResultSetDao rs = new ResultSetDao(getResultSetOriginal(sql))) {
            try {
                if (rs.next()) {
                    object = populateGeneric.code(rs);
                }
            }
            finally {
                rs.getStatement().close();
            }
        }
        
        return object;
    }
    
    public final List<HashMap<String, Object>> getListGeneric() throws SQLException {
        String sql = getSql();
        
        DaoMethods.Populate populateGeneric = new DaoMethods.Populate() {
            private ResultSetMetaData metaData = null;
            
            @Override
            public HashMap<String, Object> code(ResultSetDao rs) throws SQLException {
                HashMap<String, Object> map = new HashMap<>();
                
                if (metaData == null) {
                    metaData = rs.getMetaData();
                }

                int count = metaData.getColumnCount();
                for (int i = 1; i <= count; i++) {
                    map.put(metaData.getColumnName(i), rs.getObject(i));
                }
                
                return map;
            }
        };
        
        List<HashMap<String, Object>> list = new ArrayList<>();
        try (ResultSetDao rs = new ResultSetDao(getResultSetOriginal(sql))) {
            try {
                while (rs.next()) {
                    list.add((HashMap<String, Object>) populateGeneric.code(rs));
                }
            }
            finally {
                rs.getStatement().close();
            }
        }
        
        return list;
    }
    
    
    
    /*
    Métodos auxiliares da classe
    */
    private java.sql.ResultSet getResultSetOriginal(String sql) throws SQLException {
        BoladaoAuxiliar dao = new BoladaoAuxiliar(getConnection());
        if (paramsWithType != null && !paramsWithType.isEmpty()) {
            return dao.executeQuery(sql, paramsWithType);
        }
        else {
            return dao.executeQuery(sql, params.toArray());
        }
    }
    
    private String getSql() throws SQLException {
        if (columns == null) {
            throw new SQLException("É necessário informar as colunas do select que serão recuperadas no sql");
        }
        
        if (from == null) {
            throw new SQLException("É necessário informar as tabelas do from que serão utilizadas no sql");
        }
        
        String sql = columns + " from " + from + " where " + whereDefault;
        
        if (where != null) {
            sql += " and " + where;
        }
        
        if (groupBy != null) {
            sql += " group by " + groupBy;
        }
        
        if (orderBy != null) {
            sql += " order by " + orderBy;
        }
        
        sql = databaseSyntax.select(sql, limit, limitOffSet);
        
        if (debug) {
            System.out.println("sql -> \n" + sql);
        }
        
        return sql;
    }
    
    
    
    
    /*
    Getters and Setters
    */
    public BoladaoQuery params(Object... params) {
        this.params = Arrays.asList(params);
        return this;
    }

    public BoladaoQuery paramsWithType(ColumnDB... paramsWithType) {
        this.paramsWithType = Arrays.asList(paramsWithType);
        return this;
    }
    
    public BoladaoQuery param(Object param) {
        this.params.add(param);
        return this;
    }

    public BoladaoQuery paramWithType(Object value, int type) {
        this.paramsWithType.add(new ColumnDB(value, type));
        return this;
    }
    
    public BoladaoQuery select(String select) {
        this.columns = select;
        return this;
    }

    public BoladaoQuery from(String from) {
        this.from = from;
        return this;
    }

    public BoladaoQuery where(String where) {
        if (this.where == null || this.where.equals("")) {
            this.where = where;
        }
        else {
            this.where += " and " + where;
        }
        return this;
    }

    public BoladaoQuery groupBy(String groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public BoladaoQuery orderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public BoladaoQuery limit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public BoladaoQuery limitOffSet(Integer limitOffSet) {
        this.limitOffSet = limitOffSet;
        return this;
    }

    public BoladaoQuery populate(DaoMethods.Populate populate) {
        this.populate = populate;
        return this;
    }
    
    protected BoladaoQuery setCreateConnection(DaoMethods.CreateConnection createConnection) {
        this.createConnection = createConnection;
        return this;
    }
    
    public final void openConnection() throws SQLException {
        if (createConnection != null) {
            setConnection(createConnection.code());
        }
    }
    
    public final void closeConnection() throws SQLException {
        if (createConnection != null) {
            if (!getConnection().getAutoCommit()) {
                getConnection().rollback();
            }
            getConnection().close();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    private void setConnection(Connection connection) {
        this.connection = connection;
    }

    public BoladaoQuery debug() {
        this.debug = true;
        return this;
    }
}
