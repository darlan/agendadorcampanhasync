package app.tmove.boladao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    
    public String name() default "";
    public boolean custom() default false;
    public String insertValue() default "?";
    public String updateValue() default "?";
}
