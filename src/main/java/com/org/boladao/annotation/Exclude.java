package app.tmove.boladao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Remove this field from sql operations in Dao
 * By default, the field is excluded from all sql operations.
 * You can also use specific excludes.
 * excludeFromSelect - exclude the field from sql select.
 * excludeFromInsert - exclude the field from sql insert.
 * excludeFromUpdate - exclude the field from sql update.
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Exclude {
    
    public boolean excludeFromSelect() default true;
    public boolean excludeFromInsert() default true;
    public boolean excludeFromUpdate() default true;
}
