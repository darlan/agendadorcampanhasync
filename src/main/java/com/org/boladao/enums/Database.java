package app.tmove.boladao.enums;

import app.tmove.boladao.models.DaoField;
import java.util.List;

public enum Database {
    POSTGRES_v91,
    POSTGRES_v95,
    SYBASE;
    
    public String onConflict(String table, List<DaoField> primaryKeys) {
        switch(this) {
            case POSTGRES_v95:
                return " on conflict do nothing";
            case POSTGRES_v91: case SYBASE:
                String sqlPrimaryKeys = "";
                for (DaoField pk : primaryKeys) {
                    if (!sqlPrimaryKeys.equals("")) {
                        sqlPrimaryKeys += " and ";
                    }
                    
                    sqlPrimaryKeys += pk.getSqlName() + " = ?";
                }
                
                String conflict = " where not exists(select 1 from " + table + " where " + sqlPrimaryKeys + ")";
                
                return conflict;
            default:
                return null;
        }
    }
    
    public String select(String sqlDefault, Integer limit, Integer limitOffSet) {
        switch(this) {
            case POSTGRES_v91: case POSTGRES_v95: {
                String sqlLimit = "";
                if (limit != null) {
                    sqlLimit = " limit " + limit;

                    if (limitOffSet != null) {
                        sqlLimit += " offset " + limitOffSet;
                    }
                }

                return "select " + sqlDefault + " " + sqlLimit;
            }
            case SYBASE: {
                String sqlLimit = "";
                if (limit != null) {
                    sqlLimit = " top " + limit;

                    if (limitOffSet != null) {
                        sqlLimit += " start at " + limitOffSet;
                    }
                }

                return "select " + sqlLimit + " " + sqlDefault;
            }
            default:
                return null;
        }
    }
}
