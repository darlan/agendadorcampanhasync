package app.tmove.boladao.interfaces;

import app.tmove.boladao.utils.ResultSetDao;
import java.sql.Connection;
import java.sql.SQLException;

public class DaoMethods {
    
    public interface Populate {
        <T extends Object> T code(ResultSetDao rs) throws SQLException;
    }
    
    public interface CreateConnection {
        Connection code() throws SQLException;
    }
}
