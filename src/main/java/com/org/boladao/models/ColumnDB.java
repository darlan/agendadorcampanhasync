package app.tmove.boladao.models;

public class ColumnDB {

    public static final int STRING = java.sql.Types.VARCHAR;
    public static final int INTEGER = java.sql.Types.INTEGER;
    public static final int DATE = java.sql.Types.DATE;
    public static final int DATETIME = java.sql.Types.TIMESTAMP;
    public static final int DOUBLE = java.sql.Types.NUMERIC;
    public static final int ARRAY = java.sql.Types.ARRAY;
    
    private Object valor;
    private int type;
    
    public ColumnDB() {
    }

    @Override
    public String toString() {
        return "ColunaDB{" + "valor=" + valor + ", type=" + type + '}';
    }

    public ColumnDB(Object valor, int type) {
        this.valor = valor;
        this.type = type;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }   
}
