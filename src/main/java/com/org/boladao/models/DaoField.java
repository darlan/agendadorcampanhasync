package app.tmove.boladao.models;

import app.tmove.boladao.annotation.Column;
import app.tmove.boladao.annotation.Exclude;
import app.tmove.boladao.annotation.PrimaryKey;
import java.lang.reflect.Field;

public class DaoField {
    
    private Field field;
    private String name;
    private String sqlName;
    private String getMethod;
    private String setMethod;
            
    private Exclude annExclude;
    private PrimaryKey annPrimaryKey;
    private Column annColumn;

    public DaoField() {
    }

    public DaoField(Field field, String name, String sqlName, String getMethod, String setMethod, Exclude annExclude, PrimaryKey annPrimaryKey, Column annColumn) {
        this.field = field;
        this.name = name;
        this.sqlName = sqlName;
        this.getMethod = getMethod;
        this.setMethod = setMethod;
        this.annExclude = annExclude;
        this.annPrimaryKey = annPrimaryKey;
        this.annColumn = annColumn;
    }

    @Override
    public String toString() {
        return "DaoField{" + "field=" + field + ", name=" + name + ", sqlName=" + sqlName + ", getMethod=" + getMethod + '}';
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSqlName() {
        return sqlName;
    }

    public void setSqlName(String sqlName) {
        this.sqlName = sqlName;
    }

    public String getGetMethod() {
        return getMethod;
    }

    public void setGetMethod(String getMethod) {
        this.getMethod = getMethod;
    }

    public Exclude getAnnExclude() {
        return annExclude;
    }

    public void setAnnExclude(Exclude annExclude) {
        this.annExclude = annExclude;
    }

    public PrimaryKey getAnnPrimaryKey() {
        return annPrimaryKey;
    }

    public void setAnnPrimaryKey(PrimaryKey annPrimaryKey) {
        this.annPrimaryKey = annPrimaryKey;
    }

    public Column getAnnColumn() {
        return annColumn;
    }

    public void setAnnColumn(Column annColumn) {
        this.annColumn = annColumn;
    }

    public String getSetMethod() {
        return setMethod;
    }

    public void setSetMethod(String setMethod) {
        this.setMethod = setMethod;
    }
    
}
