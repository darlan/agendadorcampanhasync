package app.tmove.boladao.utils;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;

public class PreparedStatementDao implements java.sql.PreparedStatement {

    private final java.sql.PreparedStatement statement;
    
    //Variável para auxiliar o método que vai incrementar automático os parâmetros(?) no insert/update
    private int paramIndex = 0;

    public PreparedStatementDao(java.sql.PreparedStatement statement) {
        this.statement = statement;
    }
  
    /*
    Metodos que não existem no original
    */
    
    /**
     * Return the <code>PreparedStatementDao</code> original
     * 
     * @return <code>java.sql.PreparedStatement</code> used to execute the methods
     */
    public java.sql.PreparedStatement getPreparedStatement() {
        return statement;
    }
    
    /**
     * Reset the parameterIndex to 0
     */
    public void resetIndex() {
        paramIndex = 0;
    }
    
    /**
     * Increment the parameterIndex by <code>1</code>
     * 
     * @return the new incremented value
     */
    private int incIndex() {
        return ++paramIndex;
    }
    
    /**
     * Sets the designated parameter to the given Java <code>String</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>VARCHAR</code> or <code>LONGVARCHAR</code> value
     * (depending on the argument's
     * size relative to the driver's limits on <code>VARCHAR</code> values)
     * when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setStringNullable(int parameterIndex, String x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setString(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.VARCHAR);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>double</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>DOUBLE</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setDoubleNullable(int parameterIndex, Double x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setDouble(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.NUMERIC);
        }
    }
    
    /**
     * Sets the designated parameter to the given Java <code>float</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>FLOAT</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setFloatNullable(int parameterIndex, Float x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setFloat(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.NUMERIC);
        }
    }

    /**
     * Sets the designated parameter to the given Java <code>int</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>INTEGER</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setIntNullable(int parameterIndex, Integer x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setInt(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.INTEGER);
        }
    }
    
    /**
     * Sets the designated parameter to the given Java <code>long</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>LONG</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setLongNullable(int parameterIndex, Long x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setLong(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.BIGINT);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.Date</code> value
     * using the default time zone of the virtual machine that is running
     * the application, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>DATE</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setDateNullable(int parameterIndex, Date x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setDate(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.DATE);
        }
    }
    
    /**
     * Sets the designated parameter to the given <code>java.util.Date</code> value
     * using the default time zone of the virtual machine that is running
     * the application, even if the parameter is <code>NULL</code>.
     * The driver converts this
     * to an SQL <code>DATE</code> value when it sends it to the database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setDateNullable(int parameterIndex, java.util.Date x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setDate(parameterIndex, new Date(x.getTime()));
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.DATE);
        }
    }

    /**
     * Sets the designated parameter to the given <code>java.sql.Timestamp</code> value, even if the parameter is <code>NULL</code>.
     * The driver
     * converts this to an SQL <code>TIMESTAMP</code> value when it sends it to the
     * database.
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setTimestampNullable(int parameterIndex, Timestamp x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setTimestamp(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.TIMESTAMP);
        }
    }
    
    /**
     * Sets the designated parameter to the given Java <code>Boolean Object</code> value, even if the parameter is <code>NULL</code>.
     * The driver converts this to an SQL <code>BOOLEAN</code> value     
     *
     * @param parameterIndex the first parameter is 1, the second is 2, ...
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
     */
    public void setBooleanNullable(int parameterIndex, Boolean x) throws SQLException {
        if (x != null) {
            getPreparedStatement().setBoolean(parameterIndex, x);
        } else {
            getPreparedStatement().setNull(parameterIndex, Types.BOOLEAN);
        }
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setNull(int, int) 
     * 
     * @param sqlType the SQL type code defined in <code>java.sql.Types</code>
     * @throws SQLException if parameterIndex does not correspond to a parameter
 marker in the SQL statement; if a database access error occurs or
 this method is called on a closed <code>PreparedStatementDao</code>
 and SQLFeatureNotSupportedException if <code>sqlType</code> is
 a <code>ARRAY</code>, <code>BLOB</code>, <code>CLOB</code>,
 <code>DATALINK</code>, <code>JAVA_OBJECT</code>, <code>NCHAR</code>,
 <code>NCLOB</code>, <code>NVARCHAR</code>, <code>LONGNVARCHAR</code>,
  <code>REF</code>, <code>ROWID</code>, <code>SQLXML</code>
 or  <code>STRUCT</code> data type and the JDBC driver does not support
 this data type     */
    public void setNull(int sqlType) throws SQLException {
        getPreparedStatement().setNull(incIndex(), sqlType);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setBoolean(int, boolean) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setBoolean(boolean x) throws SQLException {
        getPreparedStatement().setBoolean(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setInt(int, int) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setInt(int x) throws SQLException {
        getPreparedStatement().setInt(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setLong(int, long)
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setLong(long x) throws SQLException {
        getPreparedStatement().setLong(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setFloat(int, float) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setFloat(float x) throws SQLException {
        getPreparedStatement().setFloat(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setDouble(int, double) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setDouble(double x) throws SQLException {
        getPreparedStatement().setDouble(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setBigDecimal(BigDecimal x) throws SQLException {
        getPreparedStatement().setBigDecimal(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setString(int, java.lang.String) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setString(String x) throws SQLException {
        getPreparedStatement().setString(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setDate(int, java.sql.Date) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setDate(java.util.Date x) throws SQLException {
        getPreparedStatement().setDate(incIndex(), new Date(x.getTime()));
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setTimestamp(Timestamp x) throws SQLException {
        getPreparedStatement().setTimestamp(incIndex(), x);
    }
    
    public void setTimestamp(java.util.Date x) throws SQLException {
        getPreparedStatement().setTimestamp(incIndex(), new Timestamp(x.getTime()));
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setObject(int, java.lang.Object) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setObject(Object x) throws SQLException {
        getPreparedStatement().setObject(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setBlob(Blob x) throws SQLException {
        getPreparedStatement().setBlob(incIndex(), x);
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setStringNullable(int, java.lang.String)
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setStringNullable(String x) throws SQLException {
        setStringNullable(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setDoubleNullable(int, java.lang.Double) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setDoubleNullable(Double x) throws SQLException {
        setDoubleNullable(incIndex(), x);
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setFloatNullable(int, java.lang.Float) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setFloatNullable(Float x) throws SQLException {
        setFloatNullable(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setIntNullable(int, java.lang.Integer) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setIntNullable(Integer x) throws SQLException {
        setIntNullable(incIndex(), x);
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setLongNullable(int, java.lang.Long) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setLongNullable(Long x) throws SQLException {
        setLongNullable(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setDateNullable(int, java.sql.Date) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setDateNullable(Date x) throws SQLException {
        setDateNullable(incIndex(), x);
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setDateNullable(int, java.util.Date) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setDateNullable(java.util.Date x) throws SQLException {
        setDateNullable(incIndex(), x);
    }

    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setTimestampNullable(int, java.sql.Timestamp) 
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setTimestampNullable(Timestamp x) throws SQLException {
        setTimestampNullable(incIndex(), x);
    }
    
    /**
     * 
     * Do the same as the method below, but already use a auto-increment to parameterIndex
     * @see PreparedStatementDao#setStringNullable(int, java.lang.String)
     * 
     * @param x the parameter value
     * @throws SQLException if parameterIndex does not correspond to a parameter
     * marker in the SQL statement; if a database access error occurs or
     * this method is called on a closed <code>PreparedStatementDao</code>     */
    public void setBooleanNullable(Boolean x) throws SQLException {
        setBooleanNullable(incIndex(), x);
    }

    
    /*
    Metodos padrões que só fora reutilizados para manter o javadoc
    */
    
    @Override
    public ResultSet executeQuery() throws SQLException {
        return getPreparedStatement().executeQuery();
    }

    @Override
    public int executeUpdate() throws SQLException {
        return getPreparedStatement().executeUpdate();
    }

    @Override
    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        getPreparedStatement().setNull(parameterIndex, sqlType);
    }

    @Override
    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        getPreparedStatement().setBoolean(parameterIndex, x);
    }

    @Override
    public void setInt(int parameterIndex, int x) throws SQLException {
        getPreparedStatement().setInt(parameterIndex, x);
    }

    @Override
    public void setLong(int parameterIndex, long x) throws SQLException {
        getPreparedStatement().setLong(parameterIndex, x);
    }

    @Override
    public void setFloat(int parameterIndex, float x) throws SQLException {
        getPreparedStatement().setFloat(parameterIndex, x);
    }

    @Override
    public void setDouble(int parameterIndex, double x) throws SQLException {
        getPreparedStatement().setDouble(parameterIndex, x);
    }

    @Override
    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        getPreparedStatement().setBigDecimal(parameterIndex, x);
    }

    @Override
    public void setString(int parameterIndex, String x) throws SQLException {
        getPreparedStatement().setString(parameterIndex, x);
    }

    @Override
    public void setDate(int parameterIndex, Date x) throws SQLException {
        getPreparedStatement().setDate(parameterIndex, x);
    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        getPreparedStatement().setTimestamp(parameterIndex, x);
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        getPreparedStatement().setObject(parameterIndex, x, targetSqlType);
    }

    @Override
    public void setObject(int parameterIndex, Object x) throws SQLException {
        getPreparedStatement().setObject(parameterIndex, x);
    }

    @Override
    public boolean execute() throws SQLException {
        return getPreparedStatement().execute();
    }

    @Override
    public void setBlob(int parameterIndex, Blob x) throws SQLException {
        getPreparedStatement().setBlob(parameterIndex, x);
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        return getPreparedStatement().getMetaData();
    }

    @Override
    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        getPreparedStatement().setDate(parameterIndex, x);
    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        getPreparedStatement().setTimestamp(parameterIndex, x);
    }

    @Override
    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
        getPreparedStatement().setNull(parameterIndex, sqlType, typeName);
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        return getPreparedStatement().getParameterMetaData();
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        getPreparedStatement().setBlob(parameterIndex, inputStream, length);
    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        getPreparedStatement().setBlob(parameterIndex, inputStream);
    }

    @Override
    public void addBatch() throws SQLException {
        getPreparedStatement().addBatch();
    }

    @Override
    public void addBatch(String sql) throws SQLException {
        getPreparedStatement().addBatch(sql);
    }

    @Override
    public void clearBatch() throws SQLException {
        getPreparedStatement().clearBatch();
    }

    @Override
    public int[] executeBatch() throws SQLException {
        return getPreparedStatement().executeBatch();
    }
    
    @Override
    public void close() throws SQLException {
        getPreparedStatement().close();
    }

    @Override
    public void cancel() throws SQLException {
        getPreparedStatement().cancel();
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return getPreparedStatement().getResultSet();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getPreparedStatement().getConnection();
    }

    @Override
    public boolean isClosed() throws SQLException {
        return getPreparedStatement().isClosed();
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        return getPreparedStatement().getGeneratedKeys();
    }
    
    
    
    
    /*
    Metodos que nao precisaram ser implementados.
    Caso precise ser usado, jogar aqui pra cima e utilizar o metodo com o getPreparedStatement()
    */
    
    @Override
    public void setByte(int parameterIndex, byte x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setShort(int parameterIndex, short x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setTime(int parameterIndex, Time x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void clearParameters() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setRef(int parameterIndex, Ref x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setClob(int parameterIndex, Clob x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setArray(int parameterIndex, Array x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setURL(int parameterIndex, URL x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public ResultSet executeQuery(String sql) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int executeUpdate(String sql) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setMaxFieldSize(int max) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getMaxRows() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setMaxRows(int max) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setEscapeProcessing(boolean enable) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getQueryTimeout() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setQueryTimeout(int seconds) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void clearWarnings() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setCursorName(String name) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean execute(String sql) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getUpdateCount() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getFetchDirection() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setFetchSize(int rows) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getFetchSize() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getResultSetType() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean getMoreResults(int current) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean execute(String sql, String[] columnNames) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean isPoolable() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new UnsupportedOperationException("Metodo nao implementado.");
    }
    
}
