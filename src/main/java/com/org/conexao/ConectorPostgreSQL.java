package com.org.conexao;

import com.org.modelos.Cliente;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConectorPostgreSQL {

//    private final static String HOST_LOGIN = "17.1.28.218:5434";
//    private final static String SENHA = "sql";

    private final static String HOST_LOGIN = "52.205.159.217:5432";    
    private final static String SENHA = "p2L9@jX0";
    
    
    private final static String USUARIO = "postgres";

    public static Connection getConnection(Cliente cliente) {

        if (cliente == null) {
            return null;
        }

        Connection conn = null;

        try {
            DriverManager.registerDriver((Driver) Class.forName("org.postgresql.Driver").newInstance());
            DriverManager.setLoginTimeout(10);
            conn = DriverManager.getConnection("jdbc:postgresql://" + cliente.getHostBanco() + ":" + cliente.getPortaBanco() + "/" + cliente.getNmBanco(), USUARIO, SENHA);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConectorPostgreSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException e) {
            System.out.println("Problemas ao conectar no banco de dados " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("O driver não foi configurado corretamente");
        }

        return conn;
    }

    public static Connection getConnectionLogin() {
        Connection conn = null;

        try {
            DriverManager.registerDriver((Driver) Class.forName("org.postgresql.Driver").newInstance());
            DriverManager.setLoginTimeout(10);
            conn = DriverManager.getConnection("jdbc:postgresql://" + HOST_LOGIN + "/login", USUARIO, SENHA);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConectorPostgreSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException e) {
            System.out.println("Problemas ao conectar no banco de dados " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("O driver não foi configurado corretamente");
        }

        return conn;
    }

    public static Boolean isOficial() {
        return SENHA.equals("p2L9@jX0");
    }

}
