package com.org.daos;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.modelos.Cliente;
import java.sql.Connection;
import java.sql.SQLException;

public class ClienteDao extends Boladao<Cliente>{

    public ClienteDao(Connection connection) {
        super(connection, "cliente");
    }

    @Override
    protected Cliente populate(ResultSetDao rs) throws SQLException {
        Cliente cliente = new Cliente();
        cliente.setCdCliente(rs.getInt("cd_cliente"));
        cliente.setNmCliente(rs.getString("nm_cliente"));
        cliente.setNmBanco(rs.getString("nm_banco"));
        cliente.setIdStatus(rs.getString("id_status"));
        cliente.setHostBanco(rs.getString("host_banco"));
        cliente.setPortaBanco(rs.getString("porta_banco"));
        return cliente;
    }

    @Override
    protected void insert(PreparedStatementDao ps, Cliente object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void update(PreparedStatementDao ps, Cliente object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
