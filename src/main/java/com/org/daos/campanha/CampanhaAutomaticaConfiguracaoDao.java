package com.org.daos.campanha;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.enums.campanha.CampanhaAutomaticaConfiguracaoEnum;
import com.org.modelos.campanha.CampanhaAutomaticaConfiguracao;
import java.sql.Connection;
import java.sql.SQLException;

public class CampanhaAutomaticaConfiguracaoDao extends Boladao<CampanhaAutomaticaConfiguracao> {

    public CampanhaAutomaticaConfiguracaoDao(Connection connection) {
        super(connection, "campanha_automatica_configuracao");
    }

    @Override
    protected CampanhaAutomaticaConfiguracao populate(ResultSetDao rs) throws SQLException {
        CampanhaAutomaticaConfiguracao campanhaAutomaticaConfiguracao = new CampanhaAutomaticaConfiguracao();
        campanhaAutomaticaConfiguracao.setId(rs.getInt("id"));
        campanhaAutomaticaConfiguracao.setRepeticao(CampanhaAutomaticaConfiguracaoEnum.valueOf(rs.getString("repeticao")));
        campanhaAutomaticaConfiguracao.setPorDia(rs.getBoolean("por_dia"));
        campanhaAutomaticaConfiguracao.setInicioAgendamento(rs.getDate("inicio_agendamento"));
        campanhaAutomaticaConfiguracao.setFimAgendamento(rs.getDate("fim_agendamento"));
        campanhaAutomaticaConfiguracao.setDiasCriacaoCampanha(rs.getIntNullable("dias_criacao_campanha"));
        campanhaAutomaticaConfiguracao.setDiasCancelamentoCampanha(rs.getIntNullable("dias_cancelamento_campanha"));
        campanhaAutomaticaConfiguracao.setDiasAgendamentoAtividades(rs.getIntNullable("dias_agendamento_atividades"));
        campanhaAutomaticaConfiguracao.setIntervalo(rs.getIntNullable("intervalo"));
        campanhaAutomaticaConfiguracao.setInicioDia(rs.getIntNullable("inicio_dia"));
        campanhaAutomaticaConfiguracao.setFimDia(rs.getIntNullable("fim_dia"));
        campanhaAutomaticaConfiguracao.setInicioMes(rs.getIntNullable("inicio_mes"));
        campanhaAutomaticaConfiguracao.setFimMes(rs.getIntNullable("fim_mes"));
        campanhaAutomaticaConfiguracao.setProximoAgendamento(rs.getDate("proximo_agendamento"));
        return campanhaAutomaticaConfiguracao;
    }

    @Override
    protected void insert(PreparedStatementDao ps, CampanhaAutomaticaConfiguracao object) throws SQLException {
        ps.setInt(object.getId());
        ps.setString(object.getRepeticao().toString());
        ps.setBoolean(object.getPorDia());
        ps.setDate(object.getInicioAgendamento());
        ps.setDateNullable(object.getFimAgendamento());
        ps.setDate(object.getProximoAgendamento());
        ps.setIntNullable(object.getDiasCriacaoCampanha());
        ps.setIntNullable(object.getDiasCancelamentoCampanha());
        ps.setIntNullable(object.getDiasAgendamentoAtividades());
        ps.setIntNullable(object.getIntervalo());
        ps.setIntNullable(object.getInicioDia());
        ps.setIntNullable(object.getFimDia());
        ps.setIntNullable(object.getInicioMes());
        ps.setIntNullable(object.getFimMes());
    }

    @Override
    protected void update(PreparedStatementDao ps, CampanhaAutomaticaConfiguracao object) throws SQLException {
        ps.setInt(object.getId());
        ps.setString(object.getRepeticao().toString());
        ps.setBoolean(object.getPorDia());
        ps.setDate(object.getInicioAgendamento());
        ps.setDateNullable(object.getFimAgendamento());
        ps.setDate(object.getProximoAgendamento());
        ps.setIntNullable(object.getDiasCriacaoCampanha());
        ps.setIntNullable(object.getDiasCancelamentoCampanha());
        ps.setIntNullable(object.getDiasAgendamentoAtividades());
        ps.setIntNullable(object.getIntervalo());
        ps.setIntNullable(object.getInicioDia());
        ps.setIntNullable(object.getFimDia());
        ps.setIntNullable(object.getInicioMes());
        ps.setIntNullable(object.getFimMes());
    }

}
