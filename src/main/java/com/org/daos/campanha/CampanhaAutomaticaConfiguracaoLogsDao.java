package com.org.daos.campanha;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.modelos.campanha.CampanhaAutomaticaConfiguracaoLogs;
import java.sql.Connection;
import java.sql.SQLException;

public class CampanhaAutomaticaConfiguracaoLogsDao extends Boladao<CampanhaAutomaticaConfiguracaoLogs> {

    public CampanhaAutomaticaConfiguracaoLogsDao(Connection connection) {
        super(connection, "campanha_automatica_configuracao_logs");
    }

    @Override
    protected CampanhaAutomaticaConfiguracaoLogs populate(ResultSetDao rs) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void insert(PreparedStatementDao ps, CampanhaAutomaticaConfiguracaoLogs object) throws SQLException {
        ps.setInt(object.getIdCampanha());
        ps.setString(object.getMensagem());
        ps.setString(object.getUrl());
        ps.setString(object.getCorpo());
    }

    @Override
    protected void update(PreparedStatementDao ps, CampanhaAutomaticaConfiguracaoLogs object) throws SQLException {
        ps.setInt(object.getIdCampanha());
        ps.setString(object.getMensagem());
        ps.setString(object.getUrl());
        ps.setString(object.getCorpo());
    }

}
