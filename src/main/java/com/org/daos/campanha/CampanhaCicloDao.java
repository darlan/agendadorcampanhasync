package com.org.daos.campanha;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.modelos.campanha.CampanhaCiclo;
import java.sql.Connection;
import java.sql.SQLException;


public class CampanhaCicloDao extends Boladao<CampanhaCiclo>{

    public CampanhaCicloDao(Connection connection) {
        super(connection, "campanha_ciclo");
    }

    @Override
    protected CampanhaCiclo populate(ResultSetDao rs) throws SQLException {
        CampanhaCiclo campanhaCiclo = new CampanhaCiclo();
        campanhaCiclo.setId(rs.getInt("id"));
        campanhaCiclo.setNome(rs.getString("nome"));
        campanhaCiclo.setStatus(rs.getString("status"));
        campanhaCiclo.setDataInicio(rs.getDate("data_inicio"));
        campanhaCiclo.setDataFim(rs.getDate("data_fim"));
        campanhaCiclo.setIdCampanhaPai(rs.getInt("id_campanha_pai"));
        return campanhaCiclo;
    }

    @Override
    protected void insert(PreparedStatementDao ps, CampanhaCiclo object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void update(PreparedStatementDao ps, CampanhaCiclo object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public CampanhaCiclo getCampanhaCicloGeral(Integer idCampanhaPai) throws SQLException {
        return getObject("data_inicio is null and data_fim is null and id_campanha_pai = ?", idCampanhaPai);
    }
    
}
