package com.org.daos.campanha;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.interfaces.DaoMethods;
import app.tmove.boladao.models.ColumnDB;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.enums.campanha.CampanhaAutomaticaConfiguracaoEnum;
import com.org.modelos.campanha.Campanha;
import com.org.modelos.campanha.CampanhaAutomaticaConfiguracao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CampanhaDao extends Boladao<Campanha> {

    public CampanhaDao(Connection connection) {
        super(connection, "campanha");
    }

    @Override
    protected Campanha populate(ResultSetDao rs) throws SQLException {
        Campanha campanha = new Campanha();
        campanha.setId(rs.getInt("id"));
        campanha.setNome(rs.getString("nome"));
        campanha.setIdTarefaTipo(rs.getIntNullable("id_tarefa_tipo"));
        campanha.setDataInicio(rs.getTimestamp("data_inicio"));
        campanha.setDataFim(rs.getTimestamp("data_fim"));
        campanha.setStatus(rs.getString("status"));
        campanha.setIdCampanhaPai(rs.getIntNullable("id_campanha_pai"));
        campanha.setIdCampanhaCiclo(rs.getInt("id_campanha_ciclo"));
        campanha.setAutomatica(rs.getBoolean("automatica"));
        campanha.setIdCampanhaAutomaticaConfiguracao(rs.getIntNullable("id_campanha_automatica_configuracao"));
        campanha.setIdCampanhaAutomaticaPai(rs.getIntNullable("id_campanha_automatica_pai"));
        return campanha;
    }

    @Override
    protected void insert(PreparedStatementDao ps, Campanha object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void update(PreparedStatementDao ps, Campanha object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<Campanha> getAllCampCancel() throws SQLException {
        return query()
               .select("c.*")
               .from(" campanha c inner join campanha c_pai on (c_pai.id = c.id_campanha_automatica_pai) inner join campanha_automatica_configuracao c_config on (c_config.id = c_pai.id_campanha_automatica_configuracao)")
               .where("c.status <> 'E' and c_pai.status <> 'E' and c_config.dias_cancelamento_campanha is not null and (date(c.data_fim) + c_config.dias_cancelamento_campanha) = date(now())")
               .getList();
    }

    public List<Campanha> getAllCampAutoSync() throws SQLException {
        return query()
               .select("c.*, cc.repeticao, cc.por_dia, cc.inicio_agendamento, cc.fim_agendamento, cc.dias_criacao_campanha, cc.dias_cancelamento_campanha, cc.dias_agendamento_atividades, cc.intervalo, cc.inicio_dia, cc.fim_dia, cc.inicio_mes, cc.fim_mes, cc.proximo_agendamento")
               .from(" campanha c inner join campanha_automatica_configuracao cc on (cc.id = c.id_campanha_automatica_configuracao) ")
               .where(" c.automatica and c.status <> 'E' and c.id_tarefa_tipo is not null and current_date = date(cc.proximo_agendamento - coalesce(cc.dias_criacao_campanha, 0)) and date(now()) <= coalesce(cc.fim_agendamento, date(now()))")
               .populate(new DaoMethods.Populate() {
                    @Override
                    public Campanha code(ResultSetDao rs) throws SQLException {
                        Campanha campanha = populate(rs);
                        
                        CampanhaAutomaticaConfiguracao configuracao = new CampanhaAutomaticaConfiguracao();
                        configuracao.setId(campanha.getIdCampanhaAutomaticaConfiguracao());
                        configuracao.setRepeticao(CampanhaAutomaticaConfiguracaoEnum.valueOf(rs.getString("repeticao")));
                        configuracao.setPorDia(rs.getBoolean("por_dia"));
                        configuracao.setInicioAgendamento(rs.getDate("inicio_agendamento"));
                        configuracao.setFimAgendamento(rs.getDate("fim_agendamento"));
                        configuracao.setDiasCriacaoCampanha(rs.getIntNullable("dias_criacao_campanha"));
                        configuracao.setDiasCancelamentoCampanha(rs.getIntNullable("dias_cancelamento_campanha"));
                        configuracao.setDiasAgendamentoAtividades(rs.getIntNullable("dias_agendamento_atividades"));
                        configuracao.setIntervalo(rs.getIntNullable("intervalo"));
                        configuracao.setInicioDia(rs.getIntNullable("inicio_dia"));
                        configuracao.setFimDia(rs.getIntNullable("fim_dia"));
                        configuracao.setInicioMes(rs.getIntNullable("inicio_mes"));
                        configuracao.setFimMes(rs.getIntNullable("fim_mes"));
                        configuracao.setProximoAgendamento(rs.getDate("proximo_agendamento"));
                        
                        campanha.setConfiguracao(configuracao);
                        return campanha;
                    }
                })
               .getList();
    }
    
    public boolean isExisteCampanha(Campanha campanha)throws SQLException {
        return query()
               .where("automatica = false and status <> 'E' and id_campanha_pai = ? and id_tarefa_tipo = ? and ((date(?) between campanha.data_inicio and campanha.data_fim) or (date(?) between campanha.data_inicio and campanha.data_fim))")
               .paramWithType(campanha.getIdCampanhaPai(), ColumnDB.INTEGER)
               .paramWithType(campanha.getIdTarefaTipo(), ColumnDB.INTEGER)
               .paramWithType(campanha.getDataInicio(), ColumnDB.DATE)
               .paramWithType(campanha.getDataFim(), ColumnDB.DATE)
               .getObject() != null;
    }

}
