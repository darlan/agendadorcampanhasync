package com.org.daos.chamado;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.enums.chamado.CmdChamadoStatusEnum;
import com.org.modelos.chamado.CmdChamado;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public class CmdChamadoDao extends Boladao<CmdChamado> implements Serializable {
    
    public CmdChamadoDao(Connection connection) {
        super(connection, "chamados.chamado");
    }

    @Override
    protected CmdChamado populate(ResultSetDao rs) throws SQLException {
        CmdChamado object = new CmdChamado();
        object.setId(rs.getInt("id"));
        object.setIdCategoria(rs.getInt("id_categoria"));
        object.setIdUsuario(rs.getInt("id_usuario"));
        object.setDtCriacao(rs.getTimestamp("dt_criacao"));
        object.setStatus(CmdChamadoStatusEnum.getEnum(rs.getString("status")));
        object.setIdUsuarioCriacao(rs.getInt("id_usuario_criacao"));
        object.setProtocolo(rs.getString("protocolo"));
        object.setAssunto(rs.getString("assunto"));
        object.setIdUnidade(rs.getInt("id_unidade"));
        object.setDtManutencaoStatus(rs.getDate("dt_manutencao_status"));
        return object;
    }

    @Override
    protected void insert(PreparedStatementDao ps, CmdChamado object) throws SQLException {
        ps.setInt(object.getIdCategoria());
        ps.setInt(object.getIdUsuario());
        ps.setTimestamp(object.getDtCriacao());
        ps.setString(object.getStatus().getId());
        ps.setInt(object.getIdUsuarioCriacao());
        ps.setString(object.getProtocolo());
        ps.setString(object.getAssunto());
        ps.setInt(object.getIdUnidade());
        ps.setDate(new Date());
    }

    @Override
    protected void update(PreparedStatementDao ps, CmdChamado object) throws SQLException {
        ps.setInt(object.getIdCategoria());
        ps.setInt(object.getIdUsuario());
        ps.setTimestamp(object.getDtCriacao());
        ps.setString(object.getStatus().getId());
        ps.setInt(object.getIdUsuarioCriacao());
        ps.setString(object.getProtocolo());
        ps.setString(object.getAssunto());
        ps.setInt(object.getIdUnidade());
        ps.setDate(object.getDtManutencaoStatus());
        ps.setInt(object.getId());
    }
    
}
