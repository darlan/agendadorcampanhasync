package com.org.daos.taxaConversao;

import app.tmove.boladao.Boladao;
import app.tmove.boladao.utils.PreparedStatementDao;
import app.tmove.boladao.utils.ResultSetDao;
import com.org.modelos.taxaConversao.TaxaConversaoVez;
import java.sql.Connection;
import java.sql.SQLException;


public class TaxaConversaoVezDao extends Boladao<TaxaConversaoVez> {
    
    private static final String TABELA = "taxa_conversao_vez";
    private static final String PK = "cd_usuario";
    
     public TaxaConversaoVezDao(Connection connection) {
        super(connection, TABELA);
    }

    @Override
    protected TaxaConversaoVez populate(ResultSetDao rs) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void insert(PreparedStatementDao ps, TaxaConversaoVez object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void update(PreparedStatementDao ps, TaxaConversaoVez object) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void updateListAttendance() throws SQLException {
        executeUpdate("update " + TABELA + " set id_status = 'O'");
    }
    
}
