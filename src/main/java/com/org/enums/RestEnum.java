package com.org.enums;


public enum RestEnum {
    
    CRIAR_CAMPANHA("/api/campanhaAutomatica/criar/"),
    CANCELAR_CAMPANHA("/api/campanhaAutomatica/cancelar/atividades/"),
    WIKI_NOTIFICACOES("/api/wiki/notificacao/vigentes"),
    KEY("GERENTEKEY"),
    TOKEN("GERENTETOKEN");
    
    private String key;
    
    RestEnum(String key){
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
}
