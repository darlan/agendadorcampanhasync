package com.org.enums.campanha;

public enum CampanhaAutomaticaConfiguracaoEnum {

    DIARIO(0, "Diario"),
    SEMANAL(1, "Semanal"),
    MENSAL(2, "Mensal"),
    ANUAL(3, "Anual"),
    PERSONALIZADA(4, "Personalizada");

    private Integer index;
    private String descricao;

    CampanhaAutomaticaConfiguracaoEnum(Integer index, String descricao) {
        this.index = index;
        this.descricao = descricao;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
