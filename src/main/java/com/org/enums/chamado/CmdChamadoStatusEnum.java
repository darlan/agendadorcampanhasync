package com.org.enums.chamado;

public enum CmdChamadoStatusEnum {
    
    ABERTO("AB","Aberto","#FF9800"),
    EM_ATENDIMENTO("EA","Em Atendimento","#2196F3"),
    AGUARDANDO_AVALIACAO("AA","Aguardando Avaliação", "#2196F3"),
    CONCLUIDO("CO","Concluído","#4CAF50"),
    FECHADO("FE","Fechado","#009688"),
    CANCELADO("CA","Cancelado","#F44336"),
    REABERTO("RE","Reaberto","#00BCD4");

    private final String id;
    private final String descricao;
    private final String cor;

    private CmdChamadoStatusEnum(String id, String descricao, String cor) {
        this.id = id;
        this.cor = cor;
        this.descricao = descricao;
    }
    
    public static CmdChamadoStatusEnum getEnum(String id) {

        for (CmdChamadoStatusEnum enumValue : CmdChamadoStatusEnum.values()) {
            if (enumValue.getId().toUpperCase().equals(id.toUpperCase())) {
                return enumValue;
            }
        }

        return null;
    }

    public String getId() {
        return id;
    }

    public String getCor() {
        return cor;
    }

    public String getDescricao() {
        return descricao;
    }
    
}

