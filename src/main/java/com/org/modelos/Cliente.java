package com.org.modelos;

import app.tmove.boladao.annotation.PrimaryKey;
import java.io.Serializable;
import java.util.Objects;


public class Cliente implements Serializable {
    
    @PrimaryKey
    private Integer cdCliente;
    private String nmCliente;
    private String nmBanco;
    private String idStatus;
    private String hostBanco;
    private String portaBanco;

    public Cliente() {
    }
    
    public Integer getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(Integer cdCliente) {
        this.cdCliente = cdCliente;
    }

    public String getNmCliente() {
        return nmCliente;
    }

    public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
    }

    public String getNmBanco() {
        return nmBanco;
    }

    public void setNmBanco(String nmBanco) {
        this.nmBanco = nmBanco;
    }

    public String getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(String idStatus) {
        this.idStatus = idStatus;
    }

    public String getHostBanco() {
        return hostBanco;
    }

    public void setHostBanco(String hostBanco) {
        this.hostBanco = hostBanco;
    }

    public String getPortaBanco() {
        return portaBanco;
    }

    public void setPortaBanco(String portaBanco) {
        this.portaBanco = portaBanco;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.cdCliente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.cdCliente, other.cdCliente)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + "cdCliente=" + cdCliente + ", nmCliente=" + nmCliente + ", nmBanco=" + nmBanco + ", idStatus=" + idStatus + ", hostBanco=" + hostBanco + ", portaBanco=" + portaBanco + '}';
    }
    
}
