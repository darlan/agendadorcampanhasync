package com.org.modelos.campanha;

import app.tmove.boladao.annotation.Exclude;
import app.tmove.boladao.annotation.PrimaryKey;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class Campanha implements Serializable {
    
    @PrimaryKey
    private Integer id;
    private String nome;
    private Integer idTarefaTipo;
    private Date dataInicio;
    private Date dataFim;
    private String status;
    private Integer idCampanhaPai;
    private Integer idCampanhaCiclo;
    private Boolean automatica;
    private Integer idCampanhaAutomaticaConfiguracao;
    private Integer idCampanhaAutomaticaPai;
    
    @Exclude
    private Date dataAgendamentoAtividade;
    @Exclude
    private CampanhaAutomaticaConfiguracao configuracao;

    public Campanha() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdTarefaTipo() {
        return idTarefaTipo;
    }

    public void setIdTarefaTipo(Integer idTarefaTipo) {
        this.idTarefaTipo = idTarefaTipo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdCampanhaCiclo() {
        return idCampanhaCiclo;
    }

    public void setIdCampanhaCiclo(Integer idCampanhaCiclo) {
        this.idCampanhaCiclo = idCampanhaCiclo;
    }

    public Integer getIdCampanhaPai() {
        return idCampanhaPai;
    }

    public void setIdCampanhaPai(Integer idCampanhaPai) {
        this.idCampanhaPai = idCampanhaPai;
    }

    public Boolean getAutomatica() {
        return automatica;
    }

    public void setAutomatica(Boolean automatica) {
        this.automatica = automatica;
    }

    public Integer getIdCampanhaAutomaticaConfiguracao() {
        return idCampanhaAutomaticaConfiguracao;
    }

    public void setIdCampanhaAutomaticaConfiguracao(Integer idCampanhaAutomaticaConfiguracao) {
        this.idCampanhaAutomaticaConfiguracao = idCampanhaAutomaticaConfiguracao;
    }

    public CampanhaAutomaticaConfiguracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(CampanhaAutomaticaConfiguracao configuracao) {
        this.configuracao = configuracao;
    }

    public Date getDataAgendamentoAtividade() {
        return dataAgendamentoAtividade;
    }

    public void setDataAgendamentoAtividade(Date dataAgendamentoAtividade) {
        this.dataAgendamentoAtividade = dataAgendamentoAtividade;
    }

    public Integer getIdCampanhaAutomaticaPai() {
        return idCampanhaAutomaticaPai;
    }

    public void setIdCampanhaAutomaticaPai(Integer idCampanhaAutomaticaPai) {
        this.idCampanhaAutomaticaPai = idCampanhaAutomaticaPai;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Campanha other = (Campanha) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Campanha{" + "id=" + id + ", nome=" + nome + ", idTarefaTipo=" + idTarefaTipo + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", status=" + status + ", idCampanhaPai=" + idCampanhaPai + ", idCampanhaCiclo=" + idCampanhaCiclo + ", automatica=" + automatica + ", idCampanhaAutomaticaConfiguracao=" + idCampanhaAutomaticaConfiguracao + ", configuracao=" + configuracao + '}';
    }
    
}
