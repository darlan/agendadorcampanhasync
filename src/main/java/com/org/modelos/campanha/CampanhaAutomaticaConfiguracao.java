package com.org.modelos.campanha;

import app.tmove.boladao.annotation.PrimaryKey;
import com.org.enums.campanha.CampanhaAutomaticaConfiguracaoEnum;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class CampanhaAutomaticaConfiguracao implements Serializable {
    
    @PrimaryKey
    private Integer id;
    private CampanhaAutomaticaConfiguracaoEnum repeticao;
    private Boolean porDia;
    private Date inicioAgendamento;
    private Date fimAgendamento;
    private Date proximoAgendamento;
    private Integer diasCriacaoCampanha;
    private Integer diasCancelamentoCampanha;
    private Integer diasAgendamentoAtividades;
    private Integer intervalo;
    private Integer inicioDia;
    private Integer fimDia;
    private Integer inicioMes;
    private Integer fimMes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CampanhaAutomaticaConfiguracaoEnum getRepeticao() {
        return repeticao;
    }

    public void setRepeticao(CampanhaAutomaticaConfiguracaoEnum repeticao) {
        this.repeticao = repeticao;
    }

    public Boolean getPorDia() {
        return porDia;
    }

    public void setPorDia(Boolean porDia) {
        this.porDia = porDia;
    }

    public Date getInicioAgendamento() {
        return inicioAgendamento;
    }

    public void setInicioAgendamento(Date inicioAgendamento) {
        this.inicioAgendamento = inicioAgendamento;
    }

    public Date getFimAgendamento() {
        return fimAgendamento;
    }

    public void setFimAgendamento(Date fimAgendamento) {
        this.fimAgendamento = fimAgendamento;
    }

    public Integer getDiasCriacaoCampanha() {
        return diasCriacaoCampanha;
    }

    public void setDiasCriacaoCampanha(Integer diasCriacaoCampanha) {
        this.diasCriacaoCampanha = diasCriacaoCampanha;
    }

    public Integer getDiasCancelamentoCampanha() {
        return diasCancelamentoCampanha;
    }

    public void setDiasCancelamentoCampanha(Integer diasCancelamentoCampanha) {
        this.diasCancelamentoCampanha = diasCancelamentoCampanha;
    }

    public Integer getDiasAgendamentoAtividades() {
        return diasAgendamentoAtividades;
    }

    public void setDiasAgendamentoAtividades(Integer diasAgendamentoAtividades) {
        this.diasAgendamentoAtividades = diasAgendamentoAtividades;
    }

    public Integer getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(Integer intervalo) {
        this.intervalo = intervalo;
    }

    public Integer getInicioDia() {
        return inicioDia;
    }

    public void setInicioDia(Integer inicioDia) {
        this.inicioDia = inicioDia;
    }

    public Integer getFimDia() {
        return fimDia;
    }

    public void setFimDia(Integer fimDia) {
        this.fimDia = fimDia;
    }

    public Integer getInicioMes() {
        return inicioMes;
    }

    public void setInicioMes(Integer inicioMes) {
        this.inicioMes = inicioMes;
    }

    public Integer getFimMes() {
        return fimMes;
    }

    public void setFimMes(Integer fimMes) {
        this.fimMes = fimMes;
    }

    public Date getProximoAgendamento() {
        return proximoAgendamento;
    }

    public void setProximoAgendamento(Date proximoAgendamento) {
        this.proximoAgendamento = proximoAgendamento;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CampanhaAutomaticaConfiguracao other = (CampanhaAutomaticaConfiguracao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CampanhaAutomaticaConfiguracao{" + "id=" + id + ", repeticao=" + repeticao + ", porDia=" + porDia + ", inicioAgendamento=" + inicioAgendamento + ", fimAgendamento=" + fimAgendamento + ", diasCriacaoCampanha=" + diasCriacaoCampanha + ", diasCancelamentoCampanha=" + diasCancelamentoCampanha + ", diasAgendamentoAtividades=" + diasAgendamentoAtividades + ", intervalo=" + intervalo + ", inicioDia=" + inicioDia + ", fimDia=" + fimDia + ", inicioMes=" + inicioMes + ", fimMes=" + fimMes + '}';
    }
    
}
