package com.org.modelos.campanha;

import app.tmove.boladao.annotation.PrimaryKey;

public class CampanhaAutomaticaConfiguracaoLogs {

    @PrimaryKey
    private Integer idCampanha;
    private String mensagem;
    private String url;
    private String corpo;

    public CampanhaAutomaticaConfiguracaoLogs(Integer idCampanha, String mensagem, String url, String corpo) {
        this.idCampanha = idCampanha;
        this.mensagem = mensagem;
        this.url = url;
        this.corpo = corpo;
    }
    
    public Integer getIdCampanha() {
        return idCampanha;
    }

    public void setIdCampanha(Integer idCampanha) {
        this.idCampanha = idCampanha;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCorpo() {
        return corpo;
    }

    public void setCorpo(String corpo) {
        this.corpo = corpo;
    }

}
