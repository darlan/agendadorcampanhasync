package com.org.modelos.campanha;

import app.tmove.boladao.annotation.PrimaryKey;
import java.io.Serializable;
import java.util.Date;


public class CampanhaCiclo implements Serializable {
    
    @PrimaryKey
    private Integer id;
    private String nome;
    private String status;
    private Date dataInicio;
    private Date dataFim;
    private Integer idCampanhaPai;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Integer getIdCampanhaPai() {
        return idCampanhaPai;
    }

    public void setIdCampanhaPai(Integer idCampanhaPai) {
        this.idCampanhaPai = idCampanhaPai;
    }
    
    

}
