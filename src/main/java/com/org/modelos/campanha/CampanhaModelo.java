package com.org.modelos.campanha;

import java.io.Serializable;
import java.util.Date;

public class CampanhaModelo implements Serializable {

    private String nome;
    private Date inicio;
    private Date fim; 

    public CampanhaModelo(String nome, Date inicio, Date fim) {
        this.nome = nome;
        this.inicio = inicio;
        this.fim = fim;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }
    
}
