package com.org.modelos.chamado;

import app.tmove.boladao.annotation.PrimaryKey;
import com.org.enums.chamado.CmdChamadoStatusEnum;
import java.io.Serializable;
import java.util.Date;

public class CmdChamado implements Serializable {
    
    @PrimaryKey(autoIncrement = true)
    private Integer id;
    private Integer idCategoria;
    private Integer idUsuario;
    private Date dtCriacao;
    private CmdChamadoStatusEnum status;
    private Integer idUsuarioCriacao;
    private String protocolo;
    private String assunto;
    private Integer idUnidade;
    private Date dtManutencaoStatus;

    public CmdChamado() {
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getDtCriacao() {
        return dtCriacao;
    }

    public void setDtCriacao(Date dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    public CmdChamadoStatusEnum getStatus() {
        return status;
    }

    public void setStatus(CmdChamadoStatusEnum status) {
        this.status = status;
    }

    public Integer getIdUsuarioCriacao() {
        return idUsuarioCriacao;
    }

    public void setIdUsuarioCriacao(Integer idUsuarioCriacao) {
        this.idUsuarioCriacao = idUsuarioCriacao;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public Integer getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Integer idUnidade) {
        this.idUnidade = idUnidade;
    }

    public Date getDtManutencaoStatus() {
        return dtManutencaoStatus;
    }

    public void setDtManutencaoStatus(Date dtManutencaoStatus) {
        this.dtManutencaoStatus = dtManutencaoStatus;
    }

    @Override
    public String toString() {
        return "CmdChamado{" + "id=" + id + ", idCategoria=" + idCategoria + ", idUsuario=" + idUsuario + ", dtCriacao=" + dtCriacao + ", status=" + status + ", idUsuarioCriacao=" + idUsuarioCriacao + ", protocolo=" + protocolo + ", assunto=" + assunto + ", idUnidade=" + idUnidade + ", dtManutencaoStatus=" + dtManutencaoStatus + '}';
    }
    
}
