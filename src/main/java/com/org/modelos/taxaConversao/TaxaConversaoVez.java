package com.org.modelos.taxaConversao;

import java.io.Serializable;
import java.util.Date;

public class TaxaConversaoVez implements Serializable {
    
    private Integer cdUsuario;
    private Integer cdEmpresa;
    private String idStatus;
    private Date hrAtendimento;
    private Date hrAtendimentoFim;

    public TaxaConversaoVez() {
    }

    public TaxaConversaoVez(Integer cdUsuario, Integer cdEmpresa, String idStatus, Date hrAtendimento, Date hrAtendimentoFim) {
        this.cdUsuario = cdUsuario;
        this.cdEmpresa = cdEmpresa;
        this.idStatus = idStatus;
        this.hrAtendimento = hrAtendimento;
        this.hrAtendimentoFim = hrAtendimentoFim;
    }    

    public Integer getCdUsuario() {
        return cdUsuario;
    }

    public void setCdUsuario(Integer cdUsuario) {
        this.cdUsuario = cdUsuario;
    }

    public Integer getCdEmpresa() {
        return cdEmpresa;
    }

    public void setCdEmpresa(Integer cdEmpresa) {
        this.cdEmpresa = cdEmpresa;
    }

    public String getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(String idStatus) {
        this.idStatus = idStatus;
    }

    public Date getHrAtendimento() {
        return hrAtendimento;
    }

    public void setHrAtendimento(Date hrAtendimento) {
        this.hrAtendimento = hrAtendimento;
    }

    public Date getHrAtendimentoFim() {
        return hrAtendimentoFim;
    }

    public void setHrAtendimentoFim(Date hrAtendimentoFim) {
        this.hrAtendimentoFim = hrAtendimentoFim;
    }    
    
}
