package com.org.principal;

import com.org.agendadores.AgendadorChamado;
import com.org.agendadores.AgendadorFilaTaxaConversao;
import com.org.agendadores.AgendadorTarefa;
import com.org.agendadores.AgendadorWikiVigentes;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class Principal {

    public static void main(String args[]) {
        SchedulerFactory shedFact = new StdSchedulerFactory();
        try {
            Scheduler scheduler = shedFact.getScheduler();
            scheduler.start();
            scheduler.scheduleJob(AgendadorTarefa.getJob(), AgendadorTarefa.getTrigger());
            scheduler.scheduleJob(AgendadorWikiVigentes.getJob(), AgendadorWikiVigentes.getTrigger());
            scheduler.scheduleJob(AgendadorChamado.getJob(), AgendadorChamado.getTrigger());
            scheduler.scheduleJob(AgendadorFilaTaxaConversao.getJob(), AgendadorFilaTaxaConversao.getTrigger());
            System.out.println("Rodando Scheduler... :D");
        } catch (SchedulerException e) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
