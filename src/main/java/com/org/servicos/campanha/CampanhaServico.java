package com.org.servicos.campanha;

import com.org.agendadores.AgendadorTarefa;
import com.org.daos.campanha.CampanhaAutomaticaConfiguracaoDao;
import com.org.daos.campanha.CampanhaAutomaticaConfiguracaoLogsDao;
import com.org.daos.campanha.CampanhaCicloDao;
import com.org.daos.campanha.CampanhaDao;
import com.org.enums.RestEnum;
import com.org.modelos.campanha.Campanha;
import com.org.modelos.campanha.CampanhaAutomaticaConfiguracaoLogs;
import com.org.modelos.campanha.CampanhaCiclo;
import com.org.modelos.campanha.CampanhaModelo;
import com.org.modelos.Cliente;
import com.org.utis.UtilDate;
import com.org.utis.UtilEncrypt;
import com.org.utis.UtilJson;
import com.org.utis.campanha.UtilRepeticaoAgendador;
import com.org.utis.UtilServlet;
import java.io.IOException;
import java.net.ProtocolException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CampanhaServico {

    private final Connection connection;
    private final Cliente cliente;

    public CampanhaServico(Connection connection, Cliente cliente) {
        this.connection = connection;
        this.cliente = cliente;
    }

    /**
     * Realiza a verificação das campanhas que devem ser geradas no dia da
     * execução do scheduler!
     */
    public void criarCampanhas() {
        CampanhaDao campanhaDao = new CampanhaDao(connection);
        CampanhaAutomaticaConfiguracaoDao campanhaAutomaticaConfiguracaoDao = new CampanhaAutomaticaConfiguracaoDao(connection);
        CampanhaAutomaticaConfiguracaoLogsDao logsDao = new CampanhaAutomaticaConfiguracaoLogsDao(connection);
        CampanhaCicloDao campanhaCicloDao = new CampanhaCicloDao(connection);
        try {
            for (Campanha campanha : campanhaDao.getAllCampAutoSync()) {
                Calendar dtBaseCampanha = Calendar.getInstance();
                Campanha campanhaAuto = new Campanha();
                CampanhaCiclo campanhaCiclo = campanhaCicloDao.getCampanhaCicloGeral(campanha.getIdCampanhaPai());
                try {
                    Date inicioAgendamento = campanha.getConfiguracao().getInicioAgendamento();
                    Integer diasCriacaoCampanha = campanha.getConfiguracao().getDiasCriacaoCampanha();
                    
                    if (diasCriacaoCampanha != null){
                        dtBaseCampanha.add(Calendar.DAY_OF_MONTH, diasCriacaoCampanha);
                    }
                    
                    if (dtBaseCampanha.getTime().before(inicioAgendamento)) {
                        dtBaseCampanha.setTime(inicioAgendamento);
                    }
                    
                    UtilDate.retirarHoraCalendar(dtBaseCampanha);
                    CampanhaModelo campanhaModelo = null;
                    switch (campanha.getConfiguracao().getRepeticao()) {
                        case DIARIO:
                            campanhaModelo = UtilRepeticaoAgendador.getCampanhaModeloDiario(campanha, dtBaseCampanha);
                            break;
                        case SEMANAL:
                            campanhaModelo = UtilRepeticaoAgendador.getCampanhaModeloSemanal(campanha, dtBaseCampanha);
                            break;
                        case MENSAL:
                            campanhaModelo = UtilRepeticaoAgendador.getCampanhaModeloMensal(campanha, dtBaseCampanha);
                            break;
                        case ANUAL:
                            campanhaModelo = UtilRepeticaoAgendador.getCampanhaModeloAnual(campanha, dtBaseCampanha);
                            break;
                        case PERSONALIZADA:
                            campanhaModelo = UtilRepeticaoAgendador.getCampanhaModeloPersonalizada(campanha, dtBaseCampanha);
                            break;
                    }

                    if (campanhaModelo == null) {
                        continue;
                    }

                    int repeticoes = 1;
                    if (campanha.getConfiguracao().getPorDia()) {
                        int diffDias = UtilDate.diferencaDiasData(campanhaModelo.getFim(), campanhaModelo.getInicio());
                        if (diffDias > 0) {
                            repeticoes = diffDias + 1;
                        }
                    }

                    for (int i = 0; i < repeticoes; i++) {
                        String nome = campanhaModelo.getNome();
                        Calendar inicio = UtilDate.getCalendarFromDate(campanhaModelo.getInicio());
                        Calendar fim = UtilDate.getCalendarFromDate(campanhaModelo.getFim());
                        Date agendamento = null;

                        if (repeticoes > 1) {
                            fim.setTime(inicio.getTime());
                        }

                        inicio.add(Calendar.DAY_OF_MONTH, i);
                        fim.add(Calendar.DAY_OF_MONTH, i);

                        if (campanha.getConfiguracao().getDiasAgendamentoAtividades() != null) {
                            Calendar calAgendamento = UtilDate.getCalendarFromDate(fim.getTime());
                            calAgendamento.add(Calendar.DAY_OF_MONTH, (campanha.getConfiguracao().getDiasAgendamentoAtividades() * -1));
                            agendamento = calAgendamento.getTime();
                        }

                        if (repeticoes > 1) {
                            nome = "Dia " + inicio.get(Calendar.DAY_OF_MONTH) + " - " + nome;
                        }

                        campanhaAuto.setDataInicio(inicio.getTime());
                        campanhaAuto.setDataFim(fim.getTime());
                        campanhaAuto.setNome(nome);
                        campanhaAuto.setIdCampanhaCiclo(campanhaCiclo.getId());
                        campanhaAuto.setDataAgendamentoAtividade(agendamento);
                        campanhaAuto.setIdTarefaTipo(campanha.getIdTarefaTipo());
                        campanhaAuto.setIdCampanhaPai(campanha.getIdCampanhaPai());

                        if (campanhaDao.isExisteCampanha(campanhaAuto)) {
                            continue;
                        }

                        createCampaignEndPoint(campanha.getId(), campanhaAuto); //EndPoint de criação
                    }

                    campanha.getConfiguracao().setProximoAgendamento(UtilRepeticaoAgendador.getProximoAgendamento(campanha));
                    campanhaAutomaticaConfiguracaoDao.insertOrUpdate(campanha.getConfiguracao());                    
                } catch (Exception e) {
                    Logger.getLogger(AgendadorTarefa.class.getName()).log(Level.SEVERE, null, e);
                    logsDao.insertOrUpdate(new CampanhaAutomaticaConfiguracaoLogs(campanha.getId(), e.toString(), getUrlEndPointCriarCampanha(campanha.getId()), UtilJson.toJson(campanhaAuto)));
                } finally {
                    connection.commit();
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(AgendadorTarefa.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Realiza a verificação das campanhas que devem ser canceladas no dia da
     * execução do scheduler!
     */
    public void cancelarCampanhas() {
        CampanhaDao campanhaDao = new CampanhaDao(connection);
        CampanhaAutomaticaConfiguracaoLogsDao logsDao = new CampanhaAutomaticaConfiguracaoLogsDao(connection);
        try {
            for (Campanha campanha : campanhaDao.getAllCampCancel()) {
                try {
                    cancelCampaignEndPoint(campanha.getId());
                } catch (Exception ex) {
                    Logger.getLogger(CampanhaServico.class.getName()).log(Level.SEVERE, null, ex);
                    logsDao.insertOrUpdate(new CampanhaAutomaticaConfiguracaoLogs(campanha.getId(), ex.toString(), getUrlEndPointCancelarCampanha(campanha.getId()), UtilJson.toJson(campanha)));
                } finally {
                    connection.commit();
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(AgendadorTarefa.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void createCampaignEndPoint(Integer idCampanhaBase, Campanha campanhaAuto) throws IOException, ProtocolException, Exception {
        UtilServlet.callPost(getUrlEndPointCriarCampanha(idCampanhaBase), null, UtilJson.toJson(campanhaAuto), getHeadersAuth());
    }

    private void cancelCampaignEndPoint(Integer idCampanha) throws IOException, ProtocolException, Exception {
        UtilServlet.callPost(getUrlEndPointCancelarCampanha(idCampanha), null, null, getHeadersAuth());
    }

    private LinkedHashMap<String, String> getHeadersAuth() {
        LinkedHashMap<String, String> headers = new LinkedHashMap<>();
        headers.put(RestEnum.KEY.getKey(), getApiKey());
        headers.put(RestEnum.TOKEN.getKey(), getApiToken());
        return headers;
    }

    private String getUrlEndPointCriarCampanha(Integer idCampanha) {
        return UtilServlet.getUrlEndPoint() + RestEnum.CRIAR_CAMPANHA.getKey() + idCampanha;
    }

    private String getUrlEndPointCancelarCampanha(Integer idCampanha) {
        return UtilServlet.getUrlEndPoint() + RestEnum.CANCELAR_CAMPANHA.getKey() + idCampanha;
    }

    private String getApiKey() {
        return cliente.getCdCliente() + "-" + cliente.getNmBanco();
    }

    private String getApiToken() {
        return UtilEncrypt.encrypt(cliente.getNmBanco() + String.valueOf(cliente.getCdCliente()));
    }

}
