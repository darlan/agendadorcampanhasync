package com.org.servicos.chamado;

import app.tmove.boladao.models.ColumnDB;
import com.org.daos.chamado.CmdChamadoDao;
import com.org.enums.chamado.CmdChamadoStatusEnum;
import com.org.modelos.Cliente;
import com.org.modelos.chamado.CmdChamado;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChamadoServico {
    
    private final Connection connection;
    private final Cliente cliente;

    public ChamadoServico(Connection connection, Cliente cliente) {
        this.connection = connection;
        this.cliente = cliente;
    }
    
    public void fecharChamados(){
        CmdChamadoDao cmdChamadoDao = new CmdChamadoDao(connection);
        try {
            List<CmdChamado> chamados = cmdChamadoDao.query().where("status = ? and (now()::date - dt_manutencao_status::date) = 15 ").paramWithType(CmdChamadoStatusEnum.AGUARDANDO_AVALIACAO.getId(), ColumnDB.STRING).getList();
            
            chamados.forEach((chamado) -> {
                chamado.setDtManutencaoStatus(new Date());
                chamado.setStatus(CmdChamadoStatusEnum.FECHADO);
            });
            
            cmdChamadoDao.update(chamados);
        } catch (SQLException e){
            Logger.getLogger(ChamadoServico.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
}
