package com.org.utis;

import com.org.conexao.ConectorPostgreSQL;
import com.org.modelos.Cliente;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class UtilBancoDados {

    public static Connection getConnection(Class<?> classe, Cliente cliente) {
        Connection conn = ConectorPostgreSQL.getConnection(cliente);
        try {
            if (conn == null || conn.isClosed()) {
                System.out.println("Erro -> Util_Banco_Dados - getConnection() - " + classe.getName());
            } else {
                conn.setAutoCommit(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilBancoDados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    public static Connection getConnectionLogin(Class<?> classe) {
        Connection conn = ConectorPostgreSQL.getConnectionLogin();
        try {
            if (conn == null || conn.isClosed()) {
                System.out.println("Erro -> Util_Banco_Dados - getConnection() - " + classe.getName());
            } else {
                conn.setAutoCommit(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilBancoDados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

}
