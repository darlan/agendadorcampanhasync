package com.org.utis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.Days;

public class UtilDate {

    public static DateFormat getDateFormat(String format) {
        Locale localeBr = new Locale("pt", "BR");

        DateFormat sdf = new SimpleDateFormat(format, localeBr);
        sdf.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));

        return sdf;
    }

    public static String getNomeMes(int mes) {
        Locale ptBr = new Locale("pt", "BR");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, mes);
        return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, ptBr);
    }

    public static void retirarHoraCalendar(Calendar calendar) {
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
    }
    
    public static Calendar getCalendarFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static boolean isMesmoDia(Date data1, Date data2) {
        if (data1 == null || data2 == null) {
            return false;
        }
        Calendar cal = getCalendarFromDate(data1);
        Calendar cal2 = getCalendarFromDate(data2);

        return cal.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) && cal.get(Calendar.DATE) == cal2.get(Calendar.DATE);
    }
    
     public static Date normalizarMinimoData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.clear(Calendar.MILLISECOND);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MINUTE);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    }

    public static Integer diferencaDiasData(Date data1, Date data2) {

        if (data1 == null || data2 == null) {
            return null;
        }

        if (isMesmoDia(data1, data2)) {
            return 0;
        } else {
            return Days.daysBetween(new org.joda.time.LocalDate(normalizarMinimoData(data2).getTime()), new org.joda.time.LocalDate(normalizarMinimoData(data1).getTime())).getDays();
        }
    }

}
