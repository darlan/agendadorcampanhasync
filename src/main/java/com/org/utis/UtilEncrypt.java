package com.org.utis;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class UtilEncrypt {

    public static String encrypt(String string) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(string.getBytes());

            byte byteData[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();

        } catch (NoSuchAlgorithmException ex) {
            ex.getLocalizedMessage();
        }
        return null;
    }

}
