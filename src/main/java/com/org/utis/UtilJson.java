package com.org.utis;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;


public class UtilJson {

    private static Gson createGsonReader() {
        GsonBuilder gson_builder = new GsonBuilder();
        gson_builder.serializeSpecialFloatingPointValues();

        final DateFormat sdf_date = UtilDate.getDateFormat("yyyy-MM-dd");
        final DateFormat sdf_datetime = UtilDate.getDateFormat("yyyy-MM-dd HH:mm:ss");
        final DateFormat sdf_datetime2 = UtilDate.getDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        gson_builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                try {
                    int tamanhoData = json.getAsJsonPrimitive().getAsString().length();

                    switch (tamanhoData) {
                        case 10:
                            return sdf_date.parse(json.getAsJsonPrimitive().getAsString());

                        case 19:
                            return sdf_datetime.parse(json.getAsJsonPrimitive().getAsString());

                        default:
                            return sdf_datetime2.parse(json.getAsJsonPrimitive().getAsString());
                    }

                } catch (ParseException e) {
                    System.out.println("[ERRO] Erro na deserialização de datas no JSON: " + json.getAsJsonPrimitive().getAsString());
                    return null;
                }
            }
        });

        return gson_builder.create();
    }

    public static String toJson(Object object) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeSpecialFloatingPointValues();

        final DateFormat df = UtilDate.getDateFormat("yyyy-MM-dd HH:mm:ss");
        gsonBuilder.registerTypeAdapter(java.util.Date.class, new JsonSerializer<java.util.Date>() {
            @Override
            public JsonElement serialize(java.util.Date t, Type type, JsonSerializationContext jsc) {
                return new JsonPrimitive(df.format(t));
            }
        });
        gsonBuilder.registerTypeAdapter(java.sql.Date.class, new JsonSerializer<java.sql.Date>() {
            @Override
            public JsonElement serialize(java.sql.Date t, Type type, JsonSerializationContext jsc) {
                return new JsonPrimitive(df.format(t));
            }
        });
        gsonBuilder.registerTypeAdapter(java.sql.Timestamp.class, new JsonSerializer<java.sql.Timestamp>() {
            @Override
            public JsonElement serialize(java.sql.Timestamp t, Type type, JsonSerializationContext jsc) {
                return new JsonPrimitive(df.format(t));
            }
        });

        return gsonBuilder.create().toJson(object);
    }

    public static <T extends Object> T fromJson(String json, Type type) throws UnsupportedEncodingException {
        return createGsonReader().fromJson(json, type);
    }

    public static <T extends Object> T fromJson(InputStream is, Type type) throws UnsupportedEncodingException {
        StringBuilder json = new StringBuilder();

        try {
            try (InputStreamReader isr = new InputStreamReader(is, "UTF-8")) {
                try (BufferedReader in = new BufferedReader(isr)) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        json.append(inputLine);
                    }
                }
            }

        } catch (IOException ex) {
            System.out.println("[ERRO] IOException em fromJson: " + ex.getMessage());
        }

        return fromJson(json.toString(), type);
    }

    public static <T extends Object> T fromJson(JsonReader jsonReader, Type type) throws UnsupportedEncodingException {
        return createGsonReader().fromJson(jsonReader, type);
    }

    public static Gson createGsonObjectReader() {
        GsonBuilder gson_builder = new GsonBuilder().serializeNulls();
        gson_builder.serializeSpecialFloatingPointValues();
        gson_builder.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE);

        final DateFormat sdf_date = UtilDate.getDateFormat("yyyy-MM-dd");
        final DateFormat sdf_datetime = UtilDate.getDateFormat("yyyy-MM-dd HH:mm:ss");
        final DateFormat sdf_datetime2 = UtilDate.getDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        gson_builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                try {
                    int tamanhoData = json.getAsJsonPrimitive().getAsString().length();
                    switch (tamanhoData) {
                        case 10:
                            return sdf_date.parse(json.getAsJsonPrimitive().getAsString());

                        case 19:
                            return sdf_datetime.parse(json.getAsJsonPrimitive().getAsString());

                        default:
                            return sdf_datetime2.parse(json.getAsJsonPrimitive().getAsString());
                    }

                } catch (ParseException e) {
                    System.out.println("[ERRO] Erro na deserialização de datas no JSON: " + json.getAsJsonPrimitive().getAsString());
                    return null;
                }
            }
        });
        gson_builder.registerTypeAdapter(Date.class, new JsonDeserializer<Timestamp>() {
            @Override
            public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                try {
                    int tamanhoData = json.getAsJsonPrimitive().getAsString().length();

                    switch (tamanhoData) {
                        case 10:
                            return new Timestamp(sdf_date.parse(json.getAsJsonPrimitive().getAsString()).getTime());

                        case 19:
                            return new Timestamp(sdf_datetime.parse(json.getAsJsonPrimitive().getAsString()).getTime());

                        default:
                            return new Timestamp(sdf_datetime2.parse(json.getAsJsonPrimitive().getAsString()).getTime());
                    }

                } catch (ParseException e) {
                    System.out.println("[ERRO] Erro na deserialização de datas no JSON: " + json.getAsJsonPrimitive().getAsString());
                    return null;
                }
            }
        });

        return gson_builder.create();
    }

    public static class ListOfJson<T> implements ParameterizedType {

        private Class<?> wrapped;

        public ListOfJson(Class<T> wrapper) {
            this.wrapped = wrapper;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[]{wrapped};
        }

        @Override
        public Type getRawType() {
            return List.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }
    }

}
