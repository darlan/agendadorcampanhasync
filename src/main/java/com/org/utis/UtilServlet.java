package com.org.utis;

import com.org.conexao.ConectorPostgreSQL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.LinkedHashMap;

public class UtilServlet {

    public static String callGet(String link, String params) throws MalformedURLException, ProtocolException, IOException, Exception {
        return callGet(link, params, null);
    }
    
    public static String callGet(String link, String params, String body) throws MalformedURLException, ProtocolException, IOException, Exception {
        return callGet(link, params, body, null);
    }

    public static String callGet(String link, String params, String body, LinkedHashMap<String, String> headers) throws MalformedURLException, ProtocolException, IOException, Exception {
        URL url = new URL(link + (params != null && !params.equals("") ? "?" + params : ""));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        
        if (headers != null) {
            for (String header : headers.keySet()) {
                conn.setRequestProperty(header, headers.get(header));
            }
        }
        
        conn.setDoOutput(true);

        if (body != null) {
            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.flush();
        }

        if (conn.getResponseCode() != 200 && conn.getResponseCode() != 201) {
            throw new Exception("Failed : HTTP error code : " + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        StringBuilder retorno = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            retorno.append(output);
        }

        conn.disconnect();

        return retorno.toString();
    }

    public static String callPost(String link, String body) throws MalformedURLException, ProtocolException, IOException, Exception {
        return callPost(link, null, body, null);
    }

    public static String callPost(String link, String queryParams, String body, LinkedHashMap<String, String> headers) throws MalformedURLException, ProtocolException, IOException, Exception {
        URL url = new URL(link + (queryParams != null && !queryParams.equals("") ? "?" + queryParams : ""));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");

        if (headers != null) {
            for (String header : headers.keySet()) {
                conn.setRequestProperty(header, headers.get(header));
            }
        }

        conn.setDoOutput(true);

        if (body != null) {
            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.flush();
        }

        if (conn.getResponseCode() != 200 && conn.getResponseCode() != 201) {
            throw new Exception("Failed : HTTP error code : " + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        StringBuilder retorno = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            retorno.append(output);
        }

        conn.disconnect();

        return retorno.toString();
    }

    
    public static String getUrlEndPoint(){
        return ConectorPostgreSQL.isOficial() ? "https://teammove.app" : "http://localhost:8080";
    }
    
}
