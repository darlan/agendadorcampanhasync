package com.org.utis.campanha;

import com.org.modelos.campanha.Campanha;
import com.org.modelos.campanha.CampanhaAutomaticaConfiguracao;
import com.org.modelos.campanha.CampanhaModelo;
import com.org.utis.UtilDate;
import java.util.Calendar;
import java.util.Date;

public final class UtilRepeticaoAgendador {

    public static CampanhaModelo getCampanhaModeloDiario(Campanha campanha, Calendar dtBaseCampanha) {
        return new CampanhaModelo(campanha.getNome() + " - Dia " + dtBaseCampanha.get(Calendar.DAY_OF_MONTH), dtBaseCampanha.getTime(), dtBaseCampanha.getTime());
    }

    public static CampanhaModelo getCampanhaModeloSemanal(Campanha campanha, Calendar dtBaseCampanha) {
        Boolean isNormalWeek = campanha.getConfiguracao().getFimDia() >= campanha.getConfiguracao().getInicioDia();
        Integer weekOfMonth = 0;

        if (isNormalWeek) {
            if (dtBaseCampanha.get(Calendar.DAY_OF_WEEK) > campanha.getConfiguracao().getFimDia()) {
                weekOfMonth++;
            }
        } else {
            weekOfMonth--;
        }

        //Inicio 
        Calendar calInicioSemanal = Calendar.getInstance();
        calInicioSemanal.setTime(dtBaseCampanha.getTime());
        calInicioSemanal.setFirstDayOfWeek(Calendar.SUNDAY);
        calInicioSemanal.add(Calendar.WEEK_OF_MONTH, weekOfMonth);
        calInicioSemanal.set(Calendar.DAY_OF_WEEK, campanha.getConfiguracao().getInicioDia());
        
        if (!isNormalWeek) {
            weekOfMonth++;
        }

        //Fim 
        Calendar calFimSemanal = Calendar.getInstance();
        calFimSemanal.setTime(dtBaseCampanha.getTime());
        calFimSemanal.setFirstDayOfWeek(Calendar.SUNDAY);
        calFimSemanal.add(Calendar.WEEK_OF_MONTH, weekOfMonth);
        calFimSemanal.set(Calendar.DAY_OF_WEEK, campanha.getConfiguracao().getFimDia());

        UtilDate.retirarHoraCalendar(calInicioSemanal);
        UtilDate.retirarHoraCalendar(calFimSemanal);

        String nome = campanha.getNome() + " - Semana " + dtBaseCampanha.get(Calendar.WEEK_OF_MONTH);
        
        if (campanha.getConfiguracao().getPorDia()){
            nome = "Semana " + dtBaseCampanha.get(Calendar.WEEK_OF_MONTH);
        }
        
        return new CampanhaModelo(nome, calInicioSemanal.getTime(), calFimSemanal.getTime());
    }

    public static CampanhaModelo getCampanhaModeloMensal(Campanha campanha, Calendar dtBaseCampanha) {
        int adiantarMes = 0;

        if (campanha.getConfiguracao().getFimDia() != null && dtBaseCampanha.get(Calendar.DAY_OF_MONTH) > campanha.getConfiguracao().getFimDia()) {
            adiantarMes = 1;
        }

        Calendar calInicioMensal = Calendar.getInstance();
        calInicioMensal.setTime(dtBaseCampanha.getTime());
        calInicioMensal.add(Calendar.MONTH, adiantarMes);
        calInicioMensal.set(Calendar.DAY_OF_MONTH, campanha.getConfiguracao().getInicioDia() != null ? campanha.getConfiguracao().getInicioDia() : 1);

        Calendar calFimMensal = Calendar.getInstance();
        calFimMensal.setTime(dtBaseCampanha.getTime());
        calFimMensal.add(Calendar.MONTH, adiantarMes);
        calFimMensal.set(Calendar.DAY_OF_MONTH, campanha.getConfiguracao().getFimDia() != null && campanha.getConfiguracao().getFimDia() <= calFimMensal.getActualMaximum(Calendar.DAY_OF_MONTH) ? campanha.getConfiguracao().getFimDia() : calFimMensal.getActualMaximum(Calendar.DAY_OF_MONTH));

        UtilDate.retirarHoraCalendar(calInicioMensal);
        UtilDate.retirarHoraCalendar(calFimMensal);
        
        String nome = campanha.getNome() + " - Mês de " + UtilDate.getNomeMes(dtBaseCampanha.get(Calendar.MONTH));

        if (campanha.getConfiguracao().getPorDia()){
            nome = "Mês de " + UtilDate.getNomeMes(dtBaseCampanha.get(Calendar.MONTH));
        }
        
        return new CampanhaModelo(nome, calInicioMensal.getTime(), calFimMensal.getTime());
    }

    public static CampanhaModelo getCampanhaModeloAnual(Campanha campanha, Calendar dtBaseCampanha) {
        Calendar calInicioAnual = Calendar.getInstance();
        calInicioAnual.setTime(dtBaseCampanha.getTime());
        calInicioAnual.set(Calendar.DAY_OF_MONTH, campanha.getConfiguracao().getInicioDia() != null ? campanha.getConfiguracao().getInicioDia() : 1);

        if (campanha.getConfiguracao().getInicioMes() != null) {
            calInicioAnual.set(Calendar.MONTH, (campanha.getConfiguracao().getInicioMes() - 1));
        }

        Calendar calFimAnual = Calendar.getInstance();
        calFimAnual.setTime(dtBaseCampanha.getTime());
        calFimAnual.set(Calendar.DAY_OF_MONTH, campanha.getConfiguracao().getFimDia() != null && campanha.getConfiguracao().getFimDia() <= calFimAnual.getActualMaximum(Calendar.DAY_OF_MONTH) ? campanha.getConfiguracao().getFimDia() : calFimAnual.getActualMaximum(Calendar.DAY_OF_MONTH));

        if (campanha.getConfiguracao().getFimMes() != null) {
            calFimAnual.set(Calendar.MONTH, (campanha.getConfiguracao().getFimMes() - 1));
        } else {
            calFimAnual.add(Calendar.MONTH, 12);
        }

        UtilDate.retirarHoraCalendar(calInicioAnual);
        UtilDate.retirarHoraCalendar(calFimAnual);

        return new CampanhaModelo(campanha.getNome(), calInicioAnual.getTime(), calFimAnual.getTime());
    }

    public static CampanhaModelo getCampanhaModeloPersonalizada(Campanha campanha, Calendar dtBaseCampanha) {
        return new CampanhaModelo(campanha.getNome(), dtBaseCampanha.getTime(), dtBaseCampanha.getTime());
    }
    
    public static Date getProximoAgendamento(Campanha campanha){
        CampanhaAutomaticaConfiguracao configuracao = campanha.getConfiguracao();
        
        Calendar proxAgendamento = Calendar.getInstance();
        proxAgendamento.setTime(configuracao.getProximoAgendamento());
        
        switch(configuracao.getRepeticao()){
            case DIARIO:
                proxAgendamento.add(Calendar.DAY_OF_MONTH, 1);
                break;
            case SEMANAL:
                proxAgendamento.add(Calendar.DAY_OF_MONTH, 7);
                break;
            case MENSAL:
                proxAgendamento.add(Calendar.MONTH, 1);
                break;
            case ANUAL:
                proxAgendamento.add(Calendar.YEAR, 1);
                break;
            case PERSONALIZADA:
                proxAgendamento.add(Calendar.DAY_OF_MONTH, configuracao.getIntervalo());
                break;
        }        
        
        return proxAgendamento.getTime();
    }

}
