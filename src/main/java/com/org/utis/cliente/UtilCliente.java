package com.org.utis.cliente;

import app.tmove.boladao.models.ColumnDB;
import com.org.agendadores.AgendadorTarefa;
import com.org.daos.ClienteDao;
import com.org.modelos.Cliente;
import com.org.utis.UtilBancoDados;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class UtilCliente {

    public static final List<Cliente> getClientesAtivos() {
        try (Connection connection = UtilBancoDados.getConnectionLogin(AgendadorTarefa.class)) {
            return new ClienteDao(connection).query().where("id_status = ?").paramWithType("A", ColumnDB.STRING).getList();
        } catch (Exception e) {
            Logger.getLogger(AgendadorTarefa.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    

}
